<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle('Типы отчетов');?>
<?
$APPLICATION->IncludeComponent(
	"upsolutions:report.invest",
	".default",
	array(
		"SEF_FOLDER" => "/dev/",
		"SEF_MODE" => "Y",
		"COMPONENT_TEMPLATE" => ".default",
		"SEF_URL_TEMPLATES" => array(
			"sections" => "",
			"country" => "country/",
			"foundation" => "foundation/",
			"industry" => "industry/",
			"liable" => "liable/",
			"partner" => "partner/",
			"region" => "region/",
			"roadmap" => "roadmap/",
		)
	),
	false
);
?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>