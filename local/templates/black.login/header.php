<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/templates/".SITE_TEMPLATE_ID."/header.php");
CUtil::InitJSCore(array("popup", "fx"));
?><!DOCTYPE html>
<html>
<head>
	<title><?$APPLICATION->ShowTitle();?></title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="robots" content="noindex, nofollow" />
	<?if (IsModuleInstalled("bitrix24")):?>
	<meta name="apple-itunes-app" content="app-id=561683423">
	<link rel="apple-touch-icon-precomposed" href="/images/iphone/57x57.png" />
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="/images/iphone/72x72.png" />
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="/images/iphone/114x114.png" />
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="/images/iphone/144x144.png" />
	<?endif?>
	<?$APPLICATION->ShowHead();?>
</head>
<body>

	<div class="onboarding boundless">
		<div class="onboarding__background">
			<img alt="UzAFI – Государственный комитет Республики Узбекистан по инвестициям" src="<?=SITE_TEMPLATE_PATH?>/images/img.jpg">
		</div>

		<div class="onboarding__modal">
			<div class="logo-w">
				<a href="/"><img alt="UzAFI – Государственный комитет Республики Узбекистан по инвестициям" src="<?=SITE_TEMPLATE_PATH?>/images/logo.png"></a>
			</div>