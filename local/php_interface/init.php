<?php

/*
 * $var - Массив
 * $stop - Завершить работу после вывода
 * $inconsole - Выводить результаты в консоли >console.log >console.debug
 */

/**
 * @param array $var <p>
 * Принимаемый массив для вывода
 * </p>
 * @param bool $stop [optional] по умолчанию false<p>
 * Завершить работу после вывода, по умолчанию false
 * </p>
 * @param bool $inconsole [optional] по умолчанию false<p>
 * Выводить результаты в консоли >console.log >console.debug
 * </p>
 * @return string <pre></pre>
 */
function pre($var, $stop = false, $inconsole = false, $UID = false)
{
    $bt = debug_backtrace();
    $bt = $bt[0];
    $dRoot = $_SERVER["DOCUMENT_ROOT"];
    $dRoot = str_replace("/", "\\", $dRoot);
    $bt["file"] = str_replace($dRoot, "", $bt["file"]);
    $dRoot = str_replace("\\", "/", $dRoot);
    $bt["file"] = str_replace($dRoot, "", $bt["file"]);
//    if ($GLOBALS['USER']->IsAdmin()) {
    if ($UID && intval($UID) !== $GLOBALS['USER']->GetID())
        return;
    if ($inconsole) {
        echo "<script>console.log('File: " . $bt['file'] . " [" . $bt['line'] . "]');console.debug(" . json_encode($var) . ");</script>";
    } else {
        echo '<div style="padding:3px 5px; background:#99CCFF; font-weight:bold;">File: ' . $bt["file"] . ' [' . $bt["line"] . ']</div>';
        echo '<pre>';
        ((is_array($var) || is_object($var)) ? print_r($var) : var_dump($var));
        echo '</pre>';
    }
    if ($stop)
        exit(0);
//    }
}

// Переименовываем "Сделки" в "Проекты"
AddEventHandler("main", "OnEndBufferContent", "ChangeMyContent");

function ChangeMyContent(&$content)
{
    $content = renameDeals($content);
}

function renameDeals($buffer)
{
    return str_replace(
        [
            'Сделки',
            'Добавить сделку',
            'Создание сделки',
            'Создана сделка',
            'Завершить сделку',
            'Стадия сделки',
            'Мои сделки',
            'Закрытые сделки',
            'Канбан',
        ], [
        'Сводные данные',
        'Добавить проект',
        'Создание проекта',
        'Создан проект',
        'Завершить проект',
        'Стадия проекта',
        'Мои проекты',
        'Закрытые проекты',
        'Свод',
    ], $buffer);
}


function getUserDeals()
{
    global $DB, $USER;
    return false;

    $deals = [];
    $curentUser = $USER->GetID();
    $sql = 'SELECT VALUE_ID, UF_CRM_1531905843 FROM `b_uts_crm_deal`';

    $res = $DB->Query($sql);
    while ($r = $res->fetch()) {
        if ($r['UF_CRM_1531905843']) {
            $usersId = unserialize($r['UF_CRM_1531905843']);
            if (count($usersId) && in_array($curentUser, $usersId)) {
                $deals[] = $r['VALUE_ID'];
            }
        }
    }


    $sql = 'SELECT ID FROM `b_crm_deal` WHERE `ASSIGNED_BY_ID` = ' . $curentUser . ';';
    $res = $DB->Query($sql);
    while ($r = $res->fetch()) {
        if ($r['ID']) {
            $deals[] = $r['ID'];
        }
    }

    if (count($deals)) {
        return $deals;
    }
    return false;
}


function AgentChangeStage()
{

    global $DB;

    $fvalue = [];
    $fv = $DB->Query('SELECT ID, VALUE FROM b_user_field_enum WHERE USER_FIELD_ID IN (131, 134, 137, 140, 143, 146, 149, 152, 155, 158, 161, 164)');
    while ($e = $fv->GetNext()) {
        $fvalue[$e['ID']] = $e['VALUE'];
    }

    $sql = 'SELECT
VALUE_ID,
UF_CRM_1530168056178 as f1,
UF_CRM_1530190165658 as f2,
UF_CRM_1530190410989 as f3,
UF_CRM_1530190499022 as f4,
UF_CRM_1530190555857 as f5,
UF_CRM_1530245917416 as f6,
UF_CRM_1530245984072 as f7,
UF_CRM_1530246082319 as f8,
UF_CRM_1530246156008 as f9,
UF_CRM_1530246244794 as f10,
UF_CRM_1530246340671 as f11,
UF_CRM_1530246434209 as f12
FROM b_uts_crm_deal';

    $fields = $DB->Query($sql);
    $dsvalue = [];
    while ($f = $fields->GetNext()) {
        for ($i = 1; $i <= 12; $i++) {
            if ($f['f' . $i]) {
                $f['f' . $i] = $fvalue[$f['f' . $i]];
            }
        }
        $dsvalue[$f['VALUE_ID']] = $f;
    }


    $total = $DB->Query('SELECT ID, TITLE, STAGE_ID, DATE_MODIFY FROM b_crm_deal');
    $deals = [];
    $newStage = [];
    $newStageFix = [];

    $stages = [
        1 => 1,
        2 => 2,
        3 => 'PREPAYMENT_INVOICE',
        4 => 'PREPARATION',
        5 => 3,
        6 => 'EXECUTING',
        7 => 'NEW'
    ];
    $stagesRev = array_flip($stages);

    $datetime1 = new DateTime(date('Y-m-d', time()));

    while ($e = $total->GetNext()) {
        $deals[$e['STAGE_ID']][$e['ID']] = $e;
        $deals[$e['STAGE_ID']][$e['ID']]['VALUES'] = $dsvalue[$e['ID']];

        if ($e['STAGE_ID'] == $stages[6]) {
            continue;
        }

        if ($e['STAGE_ID'] != $stages[7]) {
            $datetime2 = new DateTime($e['DATE_MODIFY']);
            $difference = $datetime1->diff($datetime2);
            if ($difference->d > 50) {
                $newStage[$stages[7]][$e['ID']] = $e['ID'];
                $newStageFix[$e['ID']] = $stages[7];
                continue;
            }
        } else {
            continue;
        }

        if (
            ($dsvalue[$e['ID']]['f4'] != 'Да') &&
            ($dsvalue[$e['ID']]['f6'] != 'Да') &&
            ($dsvalue[$e['ID']]['f8'] != 'Да') &&
            ($dsvalue[$e['ID']]['f11'] != 'Да') &&
            ($dsvalue[$e['ID']]['f12'] != 'Да')
        ) {
            $newStage[$stages[1]][$e['ID']] = $e['ID'];
            $newStageFix[$e['ID']] = $stages[1];
            continue;
        }

        if ($dsvalue[$e['ID']]['f4'] == 'Да') {
            if (2 > $stagesRev[$e['STAGE_ID']]) {
                if ($e['STAGE_ID'] != $stages[2]) {
                    $newStage[$stages[2]][$e['ID']] = $e['ID'];
                    $newStageFix[$e['ID']] = $stages[2];
                }
            }
        }
        if ($dsvalue[$e['ID']]['f6'] == 'Да') {
            if (3 > $stagesRev[$e['STAGE_ID']]) {
                if ($e['STAGE_ID'] != $stages[3]) {
                    $newStage[$stages[3]][$e['ID']] = $e['ID'];
                    $newStageFix[$e['ID']] = $stages[3];
                }
            }
        }
//    elseif ($dsvalue[$e['ID']]['f6'] == 'Нет') {
//        if($e['STAGE_ID'] != $stages[4]) {
//            $newStage[$stages[4]][$e['ID']] = $e['ID'];
//            $newStageFix[$e['ID']] = $stages[4];
//        }
//    }

//    if ($dsvalue[$e['ID']]['f5']) {
//        if($e['STAGE_ID'] != $stages[4]) {
//            $newStage[$stages[4]][$e['ID']] = $e['ID'];
//            $newStageFix[$e['ID']] = $stages[4];
//        }
//    }
//    if ($dsvalue[$e['ID']]['f7']) {
//        if($e['STAGE_ID'] != $stages[4]) {
//            $newStage[$stages[4]][$e['ID']] = $e['ID'];
//            $newStageFix[$e['ID']] = $stages[4];
//        }
//    }
        if ($dsvalue[$e['ID']]['f8'] == 'Да') {
            if (4 > $stagesRev[$e['STAGE_ID']]) {
                if ($e['STAGE_ID'] != $stages[4]) {
                    $newStage[$stages[4]][$e['ID']] = $e['ID'];
                    $newStageFix[$e['ID']] = $stages[4];
                }
            }
        }
//    if ($dsvalue[$e['ID']]['f9']) {
//        if($e['STAGE_ID'] != $stages[4]) {
//            $newStage[$stages[4]][$e['ID']] = $e['ID'];
//            $newStageFix[$e['ID']] = $stages[4];
//        }
//    }
//    if ($dsvalue[$e['ID']]['f10']) {
//        if($e['STAGE_ID'] != $stages[4]) {
//            $newStage[$stages[4]][$e['ID']] = $e['ID'];
//            $newStageFix[$e['ID']] = $stages[4];
//        }
//    }
        if ($dsvalue[$e['ID']]['f11'] == 'Да') {
            if (5 > $stagesRev[$e['STAGE_ID']]) {
                if ($e['STAGE_ID'] != $stages[5]) {
                    $newStage[$stages[5]][$e['ID']] = $e['ID'];
                    $newStageFix[$e['ID']] = $stages[5];
                }
            }
        }
//    elseif ($dsvalue[$e['ID']]['f11'] == 'Нет') {
//        if($e['STAGE_ID'] != $stages[4]) {
//            $newStage[$stages[4]][$e['ID']] = $e['ID'];
//            $newStageFix[$e['ID']] = $stages[4];
//        }
//    }
        if ($dsvalue[$e['ID']]['f12'] == 'Да') {
            if (6 > $stagesRev[$e['STAGE_ID']]) {
                if ($e['STAGE_ID'] != $stages[6]) {
                    $newStage[$stages[6]][$e['ID']] = $e['ID'];
                    $newStageFix[$e['ID']] = $stages[6];
                }
            }
        }
//    elseif ($dsvalue[$e['ID']]['f12'] == 'Нет') {
//        if($e['STAGE_ID'] != $stages[5]) {
//            $newStage[$stages[5]][$e['ID']] = $e['ID'];
//            $newStageFix[$e['ID']] = $stages[5];
//        }
//    }

    }

    $newStage = [];
    if (count($newStageFix) > 0) {
        foreach ($newStageFix as $project => $stage) {
            $newStage[$stage][] = $project;
        }
    }

    if (count($newStage) > 0) {
        foreach ($newStage as $stage => $deals) {
            $deals = array_unique($deals);
            $sql = 'UPDATE `b_crm_deal` SET `STAGE_ID`=\'' . $stage . '\' WHERE  `ID` IN (' . implode(', ', $deals) . ')';
            $DB->Query($sql);
        }
    }


    return "AgentChangeStage();";
}