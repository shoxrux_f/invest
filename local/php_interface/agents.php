<?
function AgentChangeStage()
{

    global $DB;

    $fvalue = [];
    $fv = $DB->Query('SELECT ID, VALUE FROM b_user_field_enum WHERE USER_FIELD_ID IN (131, 134, 137, 140, 143, 146, 149, 152, 155, 158, 161, 164)');
    while ($e = $fv->GetNext()) {
        $fvalue[$e['ID']] = $e['VALUE'];
    }

    $sql = 'SELECT
VALUE_ID,
UF_CRM_1530168056178 as f1,
UF_CRM_1530190165658 as f2,
UF_CRM_1530190410989 as f3,
UF_CRM_1530190499022 as f4,
UF_CRM_1530190555857 as f5,
UF_CRM_1530245917416 as f6,
UF_CRM_1530245984072 as f7,
UF_CRM_1530246082319 as f8,
UF_CRM_1530246156008 as f9,
UF_CRM_1530246244794 as f10,
UF_CRM_1530246340671 as f11,
UF_CRM_1530246434209 as f12
FROM b_uts_crm_deal';

    $fields = $DB->Query($sql);
    $dsvalue = [];
    while ($f = $fields->GetNext()) {
        for ($i = 1; $i <= 12; $i++) {
            if ($f['f' . $i]) {
                $f['f' . $i] = $fvalue[$f['f' . $i]];
            }
        }
        $dsvalue[$f['VALUE_ID']] = $f;
    }


    $total = $DB->Query('SELECT ID, TITLE, STAGE_ID FROM b_crm_deal');
    $deals = [];
    $newStage = [];

    $stages = [
        1 => 1,
        2 => 2,
        3 => 'PREPAYMENT_INVOICE',
        4 => 'PREPARATION',
        5 => 3,
        6 => 'EXECUTING',
        7 => 'NEW'
    ];
    while ($e = $total->GetNext()) {
        $deals[$e['STAGE_ID']][$e['ID']] = $e;
        $deals[$e['STAGE_ID']][$e['ID']]['VALUES'] = $dsvalue[$e['ID']];

        if ($dsvalue[$e['ID']]['f4'] == 'Да') {
            $newStage[$stages[2]][] = $e['ID'];
        }
        if ($dsvalue[$e['ID']]['f6'] == 'Да') {
            $newStage[$stages[3]][] = $e['ID'];
        } elseif ($dsvalue[$e['ID']]['f6'] == 'Нет') {
            $newStage[$stages[4]][] = $e['ID'];
        }

        if ($dsvalue[$e['ID']]['f5']) {
            $newStage[$stages[4]][] = $e['ID'];
        }
        if ($dsvalue[$e['ID']]['f7']) {
            $newStage[$stages[4]][] = $e['ID'];
        }
        if ($dsvalue[$e['ID']]['f8']) {
            $newStage[$stages[4]][] = $e['ID'];
        }
        if ($dsvalue[$e['ID']]['f9']) {
            $newStage[$stages[4]][] = $e['ID'];
        }
        if ($dsvalue[$e['ID']]['f10']) {
            $newStage[$stages[4]][] = $e['ID'];
        }
        if ($dsvalue[$e['ID']]['f11'] == 'Да') {
            $newStage[$stages[5]][] = $e['ID'];
        } elseif ($dsvalue[$e['ID']]['f11'] == 'Нет') {
            $newStage[$stages[4]][] = $e['ID'];
        }
        if ($dsvalue[$e['ID']]['f12'] == 'Да') {
            $newStage[$stages[6]][] = $e['ID'];
        } elseif ($dsvalue[$e['ID']]['f12'] == 'Нет') {
            $newStage[$stages[5]][] = $e['ID'];
        }

    }

    foreach ($newStage as $stage => $deals) {
        $deals = array_unique($deals);
//    pre($deals);


        $sql = 'UPDATE `b_crm_deal` SET `STAGE_ID`=\'' . $stage . '\' WHERE  `ID` IN (' . implode(', ', $deals) . ')';
        $DB->Query($sql);

    }
    return "AgentChangeStage();";
}