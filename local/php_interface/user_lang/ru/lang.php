<?
$MESS['/bitrix/components/bitrix/crm.deal/templates/.default/lang/ru/kanban.php']["CRM_DEAL_LIST_FILTER_NAV_BUTTON_KANBAN"] = "Свод";
$MESS['/bitrix/components/bitrix/crm.deal/templates/.default/lang/ru/kanban.php']["CRM_DEAL_SHOW_TITLE"] = "Просмотреть проект";
$MESS['/bitrix/components/bitrix/crm.deal/templates/.default/lang/ru/kanban.php']["CRM_DEAL_EDIT_TITLE"] = "Редактировать проект";
$MESS['/bitrix/components/bitrix/crm.deal/templates/.default/lang/ru/kanban.php']["CRM_STAGE_INIT"] = "- Стадия проекта -";
$MESS['/bitrix/components/bitrix/crm.deal/templates/.default/lang/ru/kanban.php']["CRM_DEAL_LIST_ADD_SHORT"] = "Проект";
$MESS['/bitrix/components/bitrix/crm.deal/templates/.default/lang/ru/kanban.php']["CRM_DEAL_LIST_ADD"] = "Добавить проект";
$MESS['/bitrix/components/bitrix/crm.deal/templates/.default/lang/ru/kanban.php']["CRM_DEAL_CONV_ACCESS_DENIED"] = "Для выполнения операции необходимы разрешения на создание контактов, компаний и проектов.";
$MESS['/bitrix/components/bitrix/crm.deal/templates/.default/lang/ru/kanban.php']["CRM_DEAL_CONV_DIALOG_TITLE"] = "Создание сущности на основании проекта";
$MESS['/bitrix/components/bitrix/crm.deal/templates/.default/lang/ru/kanban.php']["CRM_DEAL_CONV_DIALOG_SYNC_LEGEND"] = "В выбранных сущностях нет полей, в которые можно передать данные из проекта. Пожалуйста, выберите сущности, в которых будут созданы недостающие поля, чтобы сохранить всю доступную информацию.";
$MESS['/bitrix/components/bitrix/crm.deal/templates/.default/lang/ru/kanban.php']["CRM_DEAL_REBUILD_SEMANTICS"] = "Для правильной работы отчётов необходимо <a id=\"#ID#\" href=\"#URL#\">выполнить обновление</a> служебных полей проектов (пользовательские данные изменены не будут).";
$MESS['/bitrix/components/bitrix/crm.deal/templates/.default/lang/ru/kanban.php']["CRM_DEAL_REBUILD_SEMANTICS_DLG_TITLE"] = "Обновление служебных полей проектов";
$MESS['/bitrix/components/bitrix/crm.deal/templates/.default/lang/ru/kanban.php']["CRM_DEAL_REBUILD_SEMANTICS_DLG_SUMMARY"] = "Будет произведено обновление служебных полей проектов. Выполнение этой операции может занять продолжительное время.";
$MESS['/bitrix/components/bitrix/crm.deal/templates/.default/lang/ru/kanban.php']["CRM_DEAL_REBUILD_SEARCH_CONTENT"] = "Для обеспечения работы быстрого поиска необходимо <a id=\"#ID#\" href=\"#URL#\">произвести перестроение</a> поискового индекса проектов.";
$MESS['/bitrix/components/bitrix/crm.deal/templates/.default/lang/ru/kanban.php']["CRM_DEAL_REBUILD_SEARCH_CONTENT_DLG_TITLE"] = "Перестроение поискового индекса для проектов";
$MESS['/bitrix/components/bitrix/crm.deal/templates/.default/lang/ru/kanban.php']["CRM_DEAL_REBUILD_SEARCH_CONTENT_DLG_SUMMARY"] = "Будет произведено перестроение поискового индекса для проектов. Выполнение этой операции может занять продолжительное время.";
$MESS['/bitrix/components/bitrix/crm.deal/templates/.default/lang/ru/kanban.php']["CRM_DEAL_MOVE_TO_CATEGORY_DLG_SUMMARY"] = "Находящиеся в работе проекты будут перенесены в первую стадию нового направления, выигранные  - в стадию \"Сделка заключена\", а проваленные в стадию \"Сделка не заключена\". Обратите внимание, что из истории будут удалены записи о изменении стадий старого направления. Кроме того, пользовательские поля, которые участвовали в расчете данных для отчетов, начнут собирать статистику заново, с момента переноса проектов.";
$MESS['/bitrix/components/bitrix/crm.deal/templates/.default/lang/ru/kanban.php']["CRM_DEAL_BUILD_TIMELINE_DLG_TITLE"] = "Подготовка истории проектов";
$MESS['/bitrix/components/bitrix/crm.deal/templates/.default/lang/ru/kanban.php']["CRM_DEAL_EXCLUDE_TITLE"] = "Добавить проект в список исключений";
$MESS['/bitrix/components/bitrix/crm.deal/templates/.default/lang/ru/kanban.php']["CRM_DEAL_CATEGORY_DLG_TITLE"] = "Настройка проекта";
$MESS['/bitrix/components/bitrix/crm.deal.list/templates/.default/lang/ru/template.php']['CRM_DEAL_LIST_FILTER_NAV_BUTTON_KANBAN'] = 'Свод';
$MESS['/bitrix/components/bitrix/crm.deal.list/templates/.default/lang/ru/template.php']['CRM_DEAL_LIST_FILTER_NAV_BUTTON_KANBAN_PII'] = 'Свод (ПИИ)';



