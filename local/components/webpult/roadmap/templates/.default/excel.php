<?
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
//require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("");
$APPLICATION->SetTitle(GetMessage("CRM_TITLE"));


$APPLICATION->RestartBuffer();

Header("Content-Type: application/force-download");
Header("Content-Type: application/octet-stream");
Header("Content-Type: application/download");
Header("Content-Disposition: attachment;filename=report.xls");
Header("Content-Transfer-Encoding: binary");

?>
<meta http-equiv="Content-type" content="text/html;charset=<? echo LANG_CHARSET ?>"/>
    <style type="text/css">
        .header {
            background: #C6EFCE;
            font-weight: bolder;
        }
        td {
            text-align: center;
            vertical-align: middle;
        }
        td.tl {
            text-align: left;
            vertical-align: top;
        }
        .grey {
            background: #F2F2F2;
        }

    </style>
    <table border="1" cellspacing="0" cellpadding="10" class="roadmap-table">
        <tbody>
        <tr class="header">
            <? if ($arResult['VIEWED_COLS'][0] == 1): ?>
                <td rowspan="4">№</td><? endif; ?>
            <? if ($arResult['VIEWED_COLS'][1] == 1): ?>
                <td rowspan="4">Наименование меморандума, соглашения, проекта</td><? endif; ?>
            <? if ($arResult['VIEWED_COLS'][2] == 1): ?>
                <td rowspan="4">Сроки реализации</td><? endif; ?>
            <? if ($arResult['VIEWED_COLS'][3] == 1): ?>
                <td rowspan="4">Страна</td><? endif; ?>
            <? if ($arResult['VIEWED_COLS'][4] == 1): ?>
                <td rowspan="4">Иностранный партнер</td><? endif; ?>
            <? if ($arResult['VIEWED_COLS'][5] == 1): ?>
                <td rowspan="4">Ответственное мин/вед</td><? endif; ?>
            <? if ($arResult['VIEWED_COLS'][6] == 1): ?>
                <td rowspan="4">Отрасль</td><? endif; ?>
            <? if ($arResult['VIEWED_COLS'][7] == 1): ?>
                <td rowspan="4">Область</td><? endif; ?>
            <? if ($arResult['VIEWED_COLS'][8] == 1): ?>
                <td rowspan="4">Район</td><? endif; ?>
            <? if ($arResult['VIEWED_COLS'][9] == 1): ?>
                <td rowspan="4">Основание реализации</td><? endif; ?>
            <? if ($arResult['VIEWED_COLS'][10] == 1): ?>
                <td rowspan="4">Статус проекта<br/> (реализовано, реализуется, реализуется с отставанием __ месяцев,
                    <br/>
                    не реализуется)
                </td><? endif; ?>
            <? if ($arResult['VIEWED_COLS'][11] == 1): ?>
                <td rowspan="4">Механизм и сроки реализации</td>
            <? endif; ?>
            <? if ($arResult['VIEWED_COLS'][12] == 1): ?>
                <td rowspan="4">Ход реализации по механизму</td>
            <? endif; ?>
            <? if ($arResult['VIEWED_COLS'][13] == 1): ?>
                <td rowspan="4">Ход реализации по факту</td>
            <? endif; ?>
            <? if ($arResult['VIEWED_COLS'][14] == 1): ?>
                <td colspan="2" rowspan="2">Заявлено<br/> по "Дорожной карте"</td>
            <? endif; ?>
            <? /* <td rowspan="4">Общая сумма проекта по всем источникам финансирования</td> */ ?>
            <? if ($arResult['VIEWED_COLS'][15] == 1): ?>
                <td colspan="11">Фактически реализуется</td>
            <? endif; ?>
            <? if ($arResult['VIEWED_COLS'][16] == 1): ?>
                <td rowspan="4">Освоение в 2017г.</td>
            <? endif; ?>
            <? if ($arResult['VIEWED_COLS'][17] == 1): ?>
                <td colspan="5" rowspan="2">Освоение в 2018г.</td>
            <? endif; ?>
            <? if ($arResult['VIEWED_COLS'][18] == 1): ?>
                <td rowspan="4">Освоение в 2019г.</td>
            <? endif; ?>
            <? if ($arResult['VIEWED_COLS'][19] == 1): ?>
                <td rowspan="4">Освоение в 2020-2025гг.</td>
            <? endif; ?>
            <? if ($arResult['VIEWED_COLS'][20] == 1): ?>
                <td rowspan="4">Ответственный сотрудник от ГКИ</td>
            <? endif; ?>
        </tr>
        <tr class="header">
            <? if ($arResult['VIEWED_COLS'][15] == 1): ?>
                <td rowspan="3">Формат реализации <br/> (ИП, СП, ГЧП (%/%), импортный контракт, прочие)</td>
                <td rowspan="3">Общая сумма</td>
                <td colspan="9">в том числе:</td>
            <? endif; ?>
        </tr>
        <tr class="header">
            <? if ($arResult['VIEWED_COLS'][14] == 1): ?>
                <td rowspan="2">Формат реализации<br/> (ИП, СП, ГЧП (%/%), импортный контракт, прочие)</td>
                <td rowspan="2">сумма проекта</td>
            <? endif; ?>
            <? if ($arResult['VIEWED_COLS'][15] == 1): ?>
                <td colspan="4">Иностранные инвестиции</td>
                <td rowspan="2">Зарубежные кредиты</td>
                <td colspan="4">Узбекская сторона</td>
            <? endif; ?>
            <? if ($arResult['VIEWED_COLS'][17] == 1): ?>
                <td rowspan="2">всего</td>
                <td rowspan="2">I квартал</td>
                <td rowspan="2">II квартал</td>
                <td rowspan="2">III квартал</td>
                <td rowspan="2">IV квартал</td>
            <? endif; ?>
        </tr>
        <tr class="header">
            <? if ($arResult['VIEWED_COLS'][15] == 1): ?>
                <td>всего</td>
                <td>ПИИ</td>
                <td>ГЧП</td>
                <td>Гранты</td>
                <td>всего</td>
                <td>Собственные средства</td>
                <td>Кредиты коммерческих банков</td>
                <td>ФРРУ</td>
            <? endif; ?>
        </tr>


        <?
        $sql = 'SELECT `ID`, `VALUE` FROM `b_user_field_enum`;';

        global $DB;
        $res = $DB->Query($sql);

        $enumsVal = [];
        while ($row = $res->fetch()) {
            $enumsVal[$row['ID']] = $row['VALUE'];
        }


        if (!\Bitrix\Main\Loader::includeModule('crm')) {
            pre('ERROR', 1);
        }

        $grey = null;
        foreach ($arResult['ITEMS'] as $item):

            echo '<tr class="' . $grey . '">';
            if ($arResult['VIEWED_COLS'][0] == 1):
                echo '<td>' . $item['ID'] . '</td>'; //ID',
            endif;
            if ($arResult['VIEWED_COLS'][1] == 1):
                echo '<td class="tl">' . $item['TITLE'] . '</td>'; //Наименование меморандума, соглашения, проекта',
            endif;
            if ($arResult['VIEWED_COLS'][2] == 1):
                echo '<td>' . $item['UF_CRM_1529661397'] . '</td>'; //Сроки реализации',
            endif;
            if ($arResult['VIEWED_COLS'][3] == 1):
                echo '<td>' . $enumsVal[$item['UF_CRM_1529991824']] . '</td>'; //Страна',
            endif;
            if ($arResult['VIEWED_COLS'][4] == 1):
                echo '<td>' . $item['COMPANY_TITLE'] . '</td>'; //Иностранный партнер',
            endif;
            if ($arResult['VIEWED_COLS'][5] == 1):
                echo '<td>' . $enumsVal[$item['UF_CRM_1530258582']] . '</td>'; //Ответственное мин/вед',
            endif;
            if ($arResult['VIEWED_COLS'][6] == 1):
                echo '<td>' . $enumsVal[$item['UF_CRM_1529910635']] . '</td>'; //Отрасль',
            endif;
            if ($arResult['VIEWED_COLS'][7] == 1):
                echo '<td>' . $enumsVal[$item['UF_CRM_1529911316']] . '</td>'; //Область',
            endif;
            if ($arResult['VIEWED_COLS'][8] == 1):
                echo '<td>' . $item['UF_CRM_1529645673714'] . '</td>'; //Район',
            endif;
            if ($arResult['VIEWED_COLS'][9] == 1):
                echo '<td>' . $enumsVal[$item['UF_CRM_1529989379347']] . '</td>'; //Основание реализации',
            endif;
            if ($arResult['VIEWED_COLS'][10] == 1):
                echo '<td>' . $arResult['DEAL_STAGE'][$item['STAGE_ID']]['NAME'] . '</td>'; //Статус проекта (реализовано, реализуется, реализуется с отставанием __ месяцев, не реализуется',
            endif;
            if ($arResult['VIEWED_COLS'][11] == 1):
                echo '<td class="tl">' . $item['UF_CRM_1529645939888'] . '</td>'; //Механизм и сроки реализации',
            endif;
            if ($arResult['VIEWED_COLS'][12] == 1):
                echo '<td class="tl">' . $item['UF_CRM_1529645950215'] . '</td>'; //Ход реализации по механизму',
            endif;
            if ($arResult['VIEWED_COLS'][13] == 1):
                echo '<td class="tl">' . $item['UF_CRM_1529645959248'] . '</td>'; //Ход реализации по факту',
            endif;
//            echo '<td>' . $item['UF_CRM_1529653486918'] . '</td>'; //Формат реализации (ИП, СП, ГЧП (%/%), импортный контракт, прочие)',
            if ($arResult['VIEWED_COLS'][14] == 1):
                $format = '';
                if (is_array($item['UF_CRM_1529653486918']) && (count($item['UF_CRM_1529653486918']) > 0)) {
                    $format = [];
                    foreach ($item['UF_CRM_1529653486918'] as $f) {
                        $format[] = $enumsVal[$f];
                    }
                    $format = implode(', ', $format);
                }
                echo '<td>' . $format . '</td>'; //Формат реализации (ИП, СП, ГЧП (%/%), импортный контракт, прочие)',


                echo '<td>' . \CCrmCurrency::MoneyToString(round($item['OPPORTUNITY_ACCOUNT']), 'USD') . '</td>'; //сумма проекта',
            endif;
//            echo '<td>' . $item['unknown2'] . '</td>'; //Общая сумма проекта по всем источникам финансирования',
//            echo '<td>' . $item['UF_CRM_1529657439220'] . '</td>'; //Формат реализации (ИП, СП, ГЧП (%/%), импортный контракт, прочие)',
            if ($arResult['VIEWED_COLS'][15] == 1):
                $format = '';
                if (is_array($item['UF_CRM_1529657439220']) && (count($item['UF_CRM_1529657439220']) > 0)) {
                    $format = [];
                    foreach ($item['UF_CRM_1529657439220'] as $f) {
                        $format[] = $enumsVal[$f];
                    }
                    $format = implode(', ', $format);
                }
                echo '<td>' . $enumsVal[$item['UF_CRM_1529657439220']] . '</td>'; //Формат реализации (ИП, СП, ГЧП (%/%), импортный контракт, прочие)',

                $total_1 = (int)$item['UF_CRM_1529657664226'] + (int)$item['UF_CRM_1529657688691'] + (int)$item['UF_CRM_1529657705660'];
                $total_2 = (int)$item['UF_CRM_1529657822923'] + (int)$item['UF_CRM_1529657837031'] + (int)$item['UF_CRM_1529657847967'];
                $total_calc = $total_1 + $total_2 + (int)$item['UF_CRM_1529657772977'];


                echo '<td>' . \CCrmCurrency::MoneyToString($total_calc, 'USD') . '</td>'; //Общая сумма', // calculator
                echo '<td>' . \CCrmCurrency::MoneyToString($total_1, 'USD') . '</td>'; //всего', // calculator
                echo '<td>' . \CCrmCurrency::MoneyToString((int)$item['UF_CRM_1529657664226'], 'USD') . '</td>'; //ПИИ',
                echo '<td>' . \CCrmCurrency::MoneyToString((int)$item['UF_CRM_1529657688691'], 'USD') . '</td>'; //ГЧП',
                echo '<td>' . \CCrmCurrency::MoneyToString((int)$item['UF_CRM_1529657705660'], 'USD') . '</td>'; //Гранты',
                echo '<td>' . \CCrmCurrency::MoneyToString((int)$item['UF_CRM_1529657772977'], 'USD') . '</td>'; //Зарубежные кредиты',
                echo '<td>' . \CCrmCurrency::MoneyToString($total_2, 'USD') . '</td>'; //всего', // calculator uz
                echo '<td>' . \CCrmCurrency::MoneyToString((int)$item['UF_CRM_1529657822923'], 'USD') . '</td>'; //Собственные средства',
                echo '<td>' . \CCrmCurrency::MoneyToString((int)$item['UF_CRM_1529657837031'], 'USD') . '</td>'; //Кредиты коммерческих банков',
                echo '<td>' . \CCrmCurrency::MoneyToString((int)$item['UF_CRM_1529657847967'], 'USD') . '</td>'; //ФРРУ',
            endif;
            if ($arResult['VIEWED_COLS'][16] == 1):
                echo '<td>' . \CCrmCurrency::MoneyToString((int)$item['UF_CRM_1530603763871'], 'USD') . '</td>'; //Освоение в 2017г.',
            endif;
            if ($arResult['VIEWED_COLS'][17] == 1):
                $total = (int)$item['UF_CRM_1530603663343'] + (int)$item['UF_CRM_1530603688894'] + (int)$item['UF_CRM_1530603706770'] + (int)$item['UF_CRM_1530603719820'];
                echo '<td>' . \CCrmCurrency::MoneyToString($total, 'USD') . '</td>'; //всего', // calculator
                echo '<td>' . \CCrmCurrency::MoneyToString((int)$item['UF_CRM_1530603663343'], 'USD') . '</td>'; //I квартал',
                echo '<td>' . \CCrmCurrency::MoneyToString((int)$item['UF_CRM_1530603688894'], 'USD') . '</td>'; //II квартал',
                echo '<td>' . \CCrmCurrency::MoneyToString((int)$item['UF_CRM_1530603706770'], 'USD') . '</td>'; //III квартал',
                echo '<td>' . \CCrmCurrency::MoneyToString((int)$item['UF_CRM_1530603719820'], 'USD') . '</td>'; //IV квартал',
            endif;
            if ($arResult['VIEWED_COLS'][18] == 1):
                echo '<td>' . \CCrmCurrency::MoneyToString((int)$item['UF_CRM_1530603783840'], 'USD') . '</td>'; //Освоение в 2019г.',
            endif;
            if ($arResult['VIEWED_COLS'][19] == 1):
                echo '<td>' . \CCrmCurrency::MoneyToString((int)$item['UF_CRM_1530603808500'], 'USD') . '</td>'; //Освоение в 2020-2025гг.',
            endif;
            if ($arResult['VIEWED_COLS'][20] == 1):
                echo '<td>' . $item['ASSIGNED_BY_FIRST_NAME'] . $item['ASSIGNED_BY_LAST_NAME'] . '</td>'; //Ответственный сотрудник от ГКИ',
            endif;
            echo '</tr>';
            if ($grey == null) {
                $grey = 'grey';
            } else {
                $grey = null;
            }
        endforeach;
        ?>
        </tbody>
    </table>

<? exit; ?>
<? //require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>