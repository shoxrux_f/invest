<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * Bitrix vars
 * @global CUser $USER
 * @global CMain $APPLICATION
 * @global CDatabase $DB
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponent $component
 */

$APPLICATION->SetAdditionalCSS("/bitrix/themes/.default/crm-entity-show.css");
if (SITE_TEMPLATE_ID === 'bitrix24') {
    $APPLICATION->SetAdditionalCSS("/bitrix/themes/.default/bitrix24/crm-entity-show.css");
}
if (CModule::IncludeModule('bitrix24') && !\Bitrix\Crm\CallList\CallList::isAvailable()) {
    CBitrix24::initLicenseInfoPopupJS();
}


CJSCore::Init(array('ajax', 'jquery', 'popup'));
//CUtil::InitJSCore(array('ajax', 'jquery'/*Если не подключена ранее*/, 'popup'));// Подключаем библиотеку
Bitrix\Main\Page\Asset::getInstance()->addJs('/local/components/webpult/roadmap/templates/.default/js/scrollbooster.min.js');
Bitrix\Main\Page\Asset::getInstance()->addJs('/local/components/webpult/roadmap/templates/.default/js/select2/js/select2.min.js');
Bitrix\Main\Page\Asset::getInstance()->addCss('/local/components/webpult/roadmap/templates/.default/js/select2/css/select2.min.css');
?>


    <style type="text/css">
        .roadmap-table .header {
            background: #C6EFCE;
            font-weight: bolder;
        }

        .roadmap-table td {
            text-align: center;
            vertical-align: middle;
        }

        .roadmap-table td.tl {
            text-align: left;
            vertical-align: top;
        }

        .roadmap-table .grey {
            background: #F2F2F2;
        }

        .workarea-content-paddings {
            height: 1000px;
        }

        .bigtext {
            height: 175px;
            overflow: hidden;
        }

        .showBigText {
            height: auto;
            display: block;
            position: absolute;
            width: 500px;
            background: #fff;
            border: 1px solid;
            padding: 20px 10px;
            line-height: 1.4;
            cursor: default;
        }

    </style>
    <table border="1" cellspacing="0" cellpadding="10" class="roadmap-table">
        <tbody>
        <tr class="header">
            <? if ($arResult['VIEWED_COLS'][0] == 1): ?>
                <td rowspan="4">№</td><? endif; ?>
            <? if ($arResult['VIEWED_COLS'][1] == 1): ?>
                <td rowspan="4">Наименование меморандума, соглашения, проекта</td><? endif; ?>
            <? if ($arResult['VIEWED_COLS'][2] == 1): ?>
                <td rowspan="4">Сроки реализации</td><? endif; ?>
            <? if ($arResult['VIEWED_COLS'][3] == 1): ?>
                <td rowspan="4">Страна</td><? endif; ?>
            <? if ($arResult['VIEWED_COLS'][4] == 1): ?>
                <td rowspan="4">Иностранный партнер</td><? endif; ?>
            <? if ($arResult['VIEWED_COLS'][5] == 1): ?>
                <td rowspan="4">Ответственное мин/вед</td><? endif; ?>
            <? if ($arResult['VIEWED_COLS'][6] == 1): ?>
                <td rowspan="4">Отрасль</td><? endif; ?>
            <? if ($arResult['VIEWED_COLS'][7] == 1): ?>
                <td rowspan="4">Область</td><? endif; ?>
            <? if ($arResult['VIEWED_COLS'][8] == 1): ?>
                <td rowspan="4">Район</td><? endif; ?>
            <? if ($arResult['VIEWED_COLS'][9] == 1): ?>
                <td rowspan="4">Основание реализации</td><? endif; ?>
            <? if ($arResult['VIEWED_COLS'][10] == 1): ?>
                <td rowspan="4">Статус проекта<br/> (реализовано, реализуется, реализуется с отставанием __ месяцев,
                    <br/>
                    не реализуется)
                </td><? endif; ?>
            <? if ($arResult['VIEWED_COLS'][11] == 1): ?>
                <td rowspan="4">Механизм и сроки реализации</td>
            <? endif; ?>
            <? if ($arResult['VIEWED_COLS'][12] == 1): ?>
                <td rowspan="4">Ход реализации по механизму</td>
            <? endif; ?>
            <? if ($arResult['VIEWED_COLS'][13] == 1): ?>
                <td rowspan="4">Ход реализации по факту</td>
            <? endif; ?>
            <? if ($arResult['VIEWED_COLS'][14] == 1): ?>
                <td colspan="2" rowspan="2">Заявлено<br/> по "Дорожной карте"</td>
            <? endif; ?>
            <? /* <td rowspan="4">Общая сумма проекта по всем источникам финансирования</td> */ ?>
            <? if ($arResult['VIEWED_COLS'][15] == 1): ?>
                <td colspan="11">Фактически реализуется</td>
            <? endif; ?>
            <? if ($arResult['VIEWED_COLS'][16] == 1): ?>
                <td rowspan="4">Освоение в 2017г.</td>
            <? endif; ?>
            <? if ($arResult['VIEWED_COLS'][17] == 1): ?>
                <td colspan="5" rowspan="2">Освоение в 2018г.</td>
            <? endif; ?>
            <? if ($arResult['VIEWED_COLS'][18] == 1): ?>
                <td rowspan="4">Освоение в 2019г.</td>
            <? endif; ?>
            <? if ($arResult['VIEWED_COLS'][19] == 1): ?>
                <td rowspan="4">Освоение в 2020-2025гг.</td>
            <? endif; ?>
            <? if ($arResult['VIEWED_COLS'][20] == 1): ?>
                <td rowspan="4">Ответственный сотрудник от ГКИ</td>
            <? endif; ?>
        </tr>
        <tr class="header">
            <? if ($arResult['VIEWED_COLS'][15] == 1): ?>
                <td rowspan="3">Формат реализации <br/> (ИП, СП, ГЧП (%/%), импортный контракт, прочие)</td>
                <td rowspan="3">Общая сумма</td>
                <td colspan="9">в том числе:</td>
            <? endif; ?>
        </tr>
        <tr class="header">
            <? if ($arResult['VIEWED_COLS'][14] == 1): ?>
                <td rowspan="2">Формат реализации<br/> (ИП, СП, ГЧП (%/%), импортный контракт, прочие)</td>
                <td rowspan="2">сумма проекта</td>
            <? endif; ?>
            <? if ($arResult['VIEWED_COLS'][15] == 1): ?>
                <td colspan="4">Иностранные инвестиции</td>
                <td rowspan="2">Зарубежные кредиты</td>
                <td colspan="4">Узбекская сторона</td>
            <? endif; ?>
            <? if ($arResult['VIEWED_COLS'][17] == 1): ?>
                <td rowspan="2">всего</td>
                <td rowspan="2">I квартал</td>
                <td rowspan="2">II квартал</td>
                <td rowspan="2">III квартал</td>
                <td rowspan="2">IV квартал</td>
            <? endif; ?>
        </tr>
        <tr class="header">
            <? if ($arResult['VIEWED_COLS'][15] == 1): ?>
                <td>всего</td>
                <td>ПИИ</td>
                <td>ГЧП</td>
                <td>Гранты</td>
                <td>всего</td>
                <td>Собственные средства</td>
                <td>Кредиты коммерческих банков</td>
                <td>ФРРУ</td>
            <? endif; ?>
        </tr>


        <?
        $sql = 'SELECT `ID`, `VALUE` FROM `b_user_field_enum`;';

        global $DB;
        $res = $DB->Query($sql);

        $enumsVal = [];
        while ($row = $res->fetch()) {
            $enumsVal[$row['ID']] = $row['VALUE'];
        }


        if (!\Bitrix\Main\Loader::includeModule('crm')) {
            pre('ERROR', 1);
        }

        $grey = null;
        foreach ($arResult['ITEMS'] as $item):


            echo '<tr class="' . $grey . '">';
            if ($arResult['VIEWED_COLS'][0] == 1):
                echo '<td>' . $item['ID'] . '</td>'; //ID',
            endif;
            if ($arResult['VIEWED_COLS'][1] == 1):
                echo '<td class="tl"><a target="_blank" href="/crm/deal/details/' . $item['ID'] . '/">' . $item['TITLE'] . '</a></td>'; //Наименование меморандума, соглашения, проекта',
            endif;
            if ($arResult['VIEWED_COLS'][2] == 1):
                echo '<td>' . $item['UF_CRM_1529661397'] . '</td>'; //Сроки реализации',
            endif;
            if ($arResult['VIEWED_COLS'][3] == 1):
                echo '<td>' . $enumsVal[$item['UF_CRM_1529991824']] . '</td>'; //Страна',
            endif;
            if ($arResult['VIEWED_COLS'][4] == 1):
                echo '<td>' . $item['COMPANY_TITLE'] . '</td>'; //Иностранный партнер',
            endif;
            if ($arResult['VIEWED_COLS'][5] == 1):
                echo '<td>' . $enumsVal[$item['UF_CRM_1530258582']] . '</td>'; //Ответственное мин/вед',
            endif;
            if ($arResult['VIEWED_COLS'][6] == 1):
                echo '<td>' . $enumsVal[$item['UF_CRM_1529910635']] . '</td>'; //Отрасль',
            endif;
            if ($arResult['VIEWED_COLS'][7] == 1):
                echo '<td>' . $enumsVal[$item['UF_CRM_1529911316']] . '</td>'; //Область',
            endif;
            if ($arResult['VIEWED_COLS'][8] == 1):
                echo '<td>' . $item['UF_CRM_1529645673714'] . '</td>'; //Район',
            endif;
            if ($arResult['VIEWED_COLS'][9] == 1):
                echo '<td>' . $enumsVal[$item['UF_CRM_1529989379347']] . '</td>'; //Основание реализации',
            endif;
            if ($arResult['VIEWED_COLS'][10] == 1):
                echo '<td>' . $arResult['DEAL_STAGE'][$item['STAGE_ID']]['NAME'] . '</td>'; //Статус проекта (реализовано, реализуется, реализуется с отставанием __ месяцев, не реализуется',
            endif;
            if ($arResult['VIEWED_COLS'][11] == 1):
                echo '<td class="tl bigtext"><div class="asd bigtext">' . $item['UF_CRM_1529645939888'] . '</div></td>'; //Механизм и сроки реализации',
            endif;
            if ($arResult['VIEWED_COLS'][12] == 1):
                echo '<td class="tl bigtext"><div class="asd bigtext">' . $item['UF_CRM_1529645950215'] . '</div></td>'; //Ход реализации по механизму',
            endif;
            if ($arResult['VIEWED_COLS'][13] == 1):
                echo '<td class="tl bigtext"><div class="asd bigtext">' . $item['UF_CRM_1529645959248'] . '</div></td>'; //Ход реализации по факту',
            endif;
//            echo '<td>' . $item['UF_CRM_1529653486918'] . '</td>'; //Формат реализации (ИП, СП, ГЧП (%/%), импортный контракт, прочие)',
            if ($arResult['VIEWED_COLS'][14] == 1):
                $format = '';
                if (is_array($item['UF_CRM_1529653486918']) && (count($item['UF_CRM_1529653486918']) > 0)) {
                    $format = [];
                    foreach ($item['UF_CRM_1529653486918'] as $f) {
                        $format[] = $enumsVal[$f];
                    }
                    $format = implode(', ', $format);
                }
                echo '<td>' . $format . '</td>'; //Формат реализации (ИП, СП, ГЧП (%/%), импортный контракт, прочие)',


                echo '<td>' . \CCrmCurrency::MoneyToString(round($item['OPPORTUNITY_ACCOUNT']), 'USD') . '</td>'; //сумма проекта',
            endif;
//            echo '<td>' . $item['unknown2'] . '</td>'; //Общая сумма проекта по всем источникам финансирования',
//            echo '<td>' . $item['UF_CRM_1529657439220'] . '</td>'; //Формат реализации (ИП, СП, ГЧП (%/%), импортный контракт, прочие)',
            if ($arResult['VIEWED_COLS'][15] == 1):
                $format = '';
                if (is_array($item['UF_CRM_1529657439220']) && (count($item['UF_CRM_1529657439220']) > 0)) {
                    $format = [];
                    foreach ($item['UF_CRM_1529657439220'] as $f) {
                        $format[] = $enumsVal[$f];
                    }
                    $format = implode(', ', $format);
                }
                echo '<td>' . $enumsVal[$item['UF_CRM_1529657439220']] . '</td>'; //Формат реализации (ИП, СП, ГЧП (%/%), импортный контракт, прочие)',

                $total_1 = (int)$item['UF_CRM_1529657664226'] + (int)$item['UF_CRM_1529657688691'] + (int)$item['UF_CRM_1529657705660'];
                $total_2 = (int)$item['UF_CRM_1529657822923'] + (int)$item['UF_CRM_1529657837031'] + (int)$item['UF_CRM_1529657847967'];
                $total_calc = $total_1 + $total_2 + (int)$item['UF_CRM_1529657772977'];


                echo '<td>' . \CCrmCurrency::MoneyToString($total_calc, 'USD') . '</td>'; //Общая сумма', // calculator
                echo '<td>' . \CCrmCurrency::MoneyToString($total_1, 'USD') . '</td>'; //всего', // calculator
                echo '<td>' . \CCrmCurrency::MoneyToString((int)$item['UF_CRM_1529657664226'], 'USD') . '</td>'; //ПИИ',
                echo '<td>' . \CCrmCurrency::MoneyToString((int)$item['UF_CRM_1529657688691'], 'USD') . '</td>'; //ГЧП',
                echo '<td>' . \CCrmCurrency::MoneyToString((int)$item['UF_CRM_1529657705660'], 'USD') . '</td>'; //Гранты',
                echo '<td>' . \CCrmCurrency::MoneyToString((int)$item['UF_CRM_1529657772977'], 'USD') . '</td>'; //Зарубежные кредиты',
                echo '<td>' . \CCrmCurrency::MoneyToString($total_2, 'USD') . '</td>'; //всего', // calculator uz
                echo '<td>' . \CCrmCurrency::MoneyToString((int)$item['UF_CRM_1529657822923'], 'USD') . '</td>'; //Собственные средства',
                echo '<td>' . \CCrmCurrency::MoneyToString((int)$item['UF_CRM_1529657837031'], 'USD') . '</td>'; //Кредиты коммерческих банков',
                echo '<td>' . \CCrmCurrency::MoneyToString((int)$item['UF_CRM_1529657847967'], 'USD') . '</td>'; //ФРРУ',
            endif;
            if ($arResult['VIEWED_COLS'][16] == 1):
                echo '<td>' . \CCrmCurrency::MoneyToString((int)$item['UF_CRM_1530603763871'], 'USD') . '</td>'; //Освоение в 2017г.',
            endif;
            if ($arResult['VIEWED_COLS'][17] == 1):
                $total = (int)$item['UF_CRM_1530603663343'] + (int)$item['UF_CRM_1530603688894'] + (int)$item['UF_CRM_1530603706770'] + (int)$item['UF_CRM_1530603719820'];
                echo '<td>' . \CCrmCurrency::MoneyToString($total, 'USD') . '</td>'; //всего', // calculator
                echo '<td>' . \CCrmCurrency::MoneyToString((int)$item['UF_CRM_1530603663343'], 'USD') . '</td>'; //I квартал',
                echo '<td>' . \CCrmCurrency::MoneyToString((int)$item['UF_CRM_1530603688894'], 'USD') . '</td>'; //II квартал',
                echo '<td>' . \CCrmCurrency::MoneyToString((int)$item['UF_CRM_1530603706770'], 'USD') . '</td>'; //III квартал',
                echo '<td>' . \CCrmCurrency::MoneyToString((int)$item['UF_CRM_1530603719820'], 'USD') . '</td>'; //IV квартал',
            endif;
            if ($arResult['VIEWED_COLS'][18] == 1):
                echo '<td>' . \CCrmCurrency::MoneyToString((int)$item['UF_CRM_1530603783840'], 'USD') . '</td>'; //Освоение в 2019г.',
            endif;
            if ($arResult['VIEWED_COLS'][19] == 1):
                echo '<td>' . \CCrmCurrency::MoneyToString((int)$item['UF_CRM_1530603808500'], 'USD') . '</td>'; //Освоение в 2020-2025гг.',
            endif;
            if ($arResult['VIEWED_COLS'][20] == 1):
                echo '<td>' . $item['ASSIGNED_BY_FIRST_NAME'] . $item['ASSIGNED_BY_LAST_NAME'] . '</td>'; //Ответственный сотрудник от ГКИ',
            endif;
            echo '</tr>';
            if ($grey == null) {
                $grey = 'grey';
            } else {
                $grey = null;
            }
        endforeach;
        ?>
        </tbody>
    </table>
    <script type="text/javascript">

        $(document).ready(function () {

            // $('.asd').each(function() {
            //     if($(this).height() > 175) {
            //         $(this).addClass('bigtext').removeClass('asd');
            //     }
            // });

            $('div.bigtext').hover(function () {
                $(this).toggleClass('showBigText')
            })
        });


        let viewport = document.querySelector('.workarea-content-paddings')
        let content = viewport.querySelector('table')


        let sb = new ScrollBooster({
            viewport,
            content,
            bounce: false,
            emulateScroll: false,
            onUpdate: (data) => {
                content.style.transform = `translate(
      ${-data.position.x}px,
      ${-data.position.y}px
    )`
            }
        })
    </script>


<? $this->SetViewTarget("pagetitle", 100); ?>

    <style type="text/css">
        .menu-popup-item {
            cursor: pointer !important;
        }

        .menu-popup-item .menu-popup-item-icon {
            width: 16px;
            height: 16px;
            background-size: cover;
        }

        .reports-title-excel-icon .menu-popup-item-icon {
            background-image: url('data:image/svg+xml;charset=US-ASCII,%3Csvg xmlns%3D"http%3A//www.w3.org/2000/svg" width%3D"16" height%3D"16" viewBox%3D"0 0 16 16"%3E%0A  %3Cpath fill%3D"%2318823A" d%3D"M11.3273082%2C8.70746558 L13.973787%2C8.70746558 L13.973787%2C7.61047879 L11.3273082%2C7.61047879 L11.3273082%2C8.70746558 Z M11.3241022%2C12.4912035 L13.9731563%2C12.4912035 L13.9731563%2C11.3796483 L11.3241022%2C11.3796483 L11.3241022%2C12.4912035 Z M11.3238219%2C10.6158767 L13.9722628%2C10.6158767 L13.9722628%2C9.50630407 L11.3238219%2C9.50630407 L11.3238219%2C10.6158767 Z M11.3249782%2C6.79737279 L13.9730161%2C6.79737279 L13.9730161%2C5.6883666 L11.3249782%2C5.6883666 L11.3249782%2C6.79737279 Z M11.3266951%2C4.86986161 L13.9711766%2C4.86986161 L13.9711766%2C3.75922687 L11.3266951%2C3.75922687 L11.3266951%2C4.86986161 Z M15.0097823%2C13.689833 C13.1586206%2C13.6898684 11.307459%2C13.6898684 9.45627988%2C13.689833 C9.33014196%2C13.689833 9.3300894%2C13.689656 9.33007188%2C13.5583808 C9.33003684%2C13.2367956 9.33268224%2C12.9151926 9.32823237%2C12.5936782 C9.32712866%2C12.5137554 9.3524088%2C12.4949386 9.42774118%2C12.495806 C9.7576444%2C12.4996649 10.0876177%2C12.4958591 10.4175385%2C12.4987444 C10.4855128%2C12.4993463 10.5082526%2C12.479786 10.5074643%2C12.4088379 C10.5039604%2C12.0932004 10.5060803%2C11.7775098 10.5056948%2C11.4618369 C10.5056598%2C11.4359748 10.5020684%2C11.4101305 10.4993354%2C11.3733642 L10.3918379%2C11.3733642 C10.0706592%2C11.3733465 9.74946296%2C11.37124 9.42831931%2C11.3748689 C9.35267159%2C11.3757008 9.32607751%2C11.3547598 9.328355%2C11.2754389 C9.33373338%2C11.0897313 9.33403121%2C10.9036342 9.32824989%2C10.7179619 C9.32572713%2C10.6372249 9.35384538%2C10.619541 9.42847698%2C10.6203729 C9.75838021%2C10.6240549 10.088371%2C10.6200543 10.4182743%2C10.6234707 C10.4883684%2C10.6241965 10.5079899%2C10.6019632 10.5073066%2C10.532803 C10.5042057%2C10.2171655 10.5043284%2C9.90145723 10.5072541%2C9.58580203 C10.5078672%2C9.5197219 10.4879655%2C9.5000023 10.4225139%2C9.50060415 C10.0896674%2C9.50361343 9.75678596%2C9.50017931 9.42395704%2C9.50338331 C9.35305701%2C9.50407367 9.32677828%2C9.48530996 9.32851268%2C9.40993648 C9.33312021%2C9.20941258 9.33315525%2C9.00867627 9.32851268%2C8.80817007 C9.32672572%2C8.7317699 9.35538706%2C8.71413909 9.42516586%2C8.71474095 C9.74923521%2C8.71760861 10.0733571%2C8.71599776 10.3974615%2C8.71598006 L10.4987923%2C8.71598006 C10.5016129%2C8.68268332 10.5056248%2C8.65705138 10.5056598%2C8.63141944 C10.5060978%2C8.31870271 10.5036626%2C8.00595057 10.5076044%2C7.69326924 C10.508568%2C7.61741782 10.48299%2C7.59941528 10.4122652%2C7.60012334 C10.0852877%2C7.60336274 9.75820502%2C7.59904354 9.43124501%2C7.60327423 C9.3496583%2C7.60433633 9.3263403%2C7.57827955 9.32837252%2C7.49759559 C9.33348812%2C7.29415092 9.33234937%2C7.09045844 9.32879298%2C6.88696067 C9.32763672%2C6.82079203 9.34888745%2C6.80080691 9.41390104%2C6.80135566 C9.74089609%2C6.80415251 10.0679262%2C6.80261247 10.3949563%2C6.80259477 L10.4995807%2C6.80259477 C10.5019282%2C6.76968747 10.5056423%2C6.74131178 10.5056773%2C6.71293609 C10.5061153%2C6.40021935 10.5037502%2C6.08746722 10.5075694%2C5.77478589 C10.5084804%2C5.69939471 10.4832178%2C5.68020616 10.4122301%2C5.68087882 C10.0823269%2C5.684012 9.75235362%2C5.68029466 9.42245039%2C5.68378188 C9.35006124%2C5.68454305 9.32691843%2C5.66328341 9.3285302%2C5.58937917 C9.3330151%2C5.38294293 9.33275231%2C5.17632968 9.32865283%2C4.96987574 C9.32719874%2C4.897069 9.34906265%2C4.87287089 9.42248543%2C4.87364976 C9.74362908%2C4.87706617 10.0648253%2C4.87510129 10.386004%2C4.87508359 L10.4980039%2C4.87508359 C10.5010172%2C4.84371633 10.5055722%2C4.81833221 10.5056073%2C4.7929481 C10.5061153%2C4.47431902 10.5036801%2C4.15567225 10.507622%2C3.83709628 C10.5085505%2C3.762838 10.481168%2C3.74926086 10.4156464%2C3.74980961 C10.082835%2C3.75250026 9.74997101%2C3.74919006 9.41715961%2C3.75250026 C9.34731073%2C3.75319062 9.3280747%2C3.73069187 9.32872291%2C3.66190339 C9.33177124%2C3.33445891 9.33166612%2C3.00696133 9.32879298%2C2.67951685 C9.32819733%2C2.61164886 9.3461895%2C2.58831813 9.41677418%2C2.58838894 C11.2942145%2C2.5901768 13.1716549%2C2.58978737 15.0490952%2C2.59001749 C15.0720453%2C2.59001749 15.0949604%2C2.59320378 15.1252861%2C2.59545188 C15.1273884%2C2.63917487 15.1310499%2C2.67972927 15.1310499%2C2.72028367 C15.1313126%2C6.33727241 15.1312951%2C9.95426115 15.1312601%2C13.5712499 C15.1312601%2C13.6896206 15.1310323%2C13.6898153 15.0097823%2C13.689833 L15.0097823%2C13.689833 Z M6.57679636%2C11.0393702 C6.19121725%2C10.9954525 5.80570823%2C10.950632 5.4197437%2C10.9100599 C5.35003498%2C10.9027137 5.31394552%2C10.8752054 5.28565208%2C10.8093023 C4.99304714%2C10.1277901 4.69685076%2C9.44783564 4.40156539%2C8.76750946 C4.39468036%2C8.75159571 4.38679674%2C8.7361422 4.37342963%2C8.70789042 C4.35967709%2C8.73658474 4.34970869%2C8.75490591 4.34186011%2C8.77409446 C4.08755204%2C9.39512023 3.83399729%2C10.0164292 3.57739421%2C10.6364814 C3.5673207%2C10.6608388 3.52681641%2C10.6907723 3.50353345%2C10.6883294 C3.10891446%2C10.647067 2.71478601%2C10.6011313 2.320605%2C10.555762 C2.31585731%2C10.555231 2.31175783%2C10.5489823 2.3007558%2C10.540043 C2.36195021%2C10.4194242 2.42360012%2C10.2971414 2.48593327%2C10.175195 C2.87689076%2C9.41029051 3.26744531%2C8.6451559 3.66031238%2C7.88124269 C3.68667871%2C7.82997881 3.68301721%2C7.79105296 3.65903348%2C7.74193098 C3.23582323%2C6.87462264 2.81404955%2C6.00657084 2.39208316%2C5.13864294 C2.37827806%2C5.11023185 2.36582194%2C5.08113039 2.34521941%2C5.03581426 C2.43563578%2C5.03062768 2.50788478%2C5.02524639 2.58023889%2C5.02257345 C2.9010672%2C5.01069567 3.2219831%2C5.0011722 3.54268877%2C4.98681619 C3.60137794%2C4.98417865 3.6253967%2C5.00936804 3.64568388%2C5.05813599 C3.90007955%2C5.66954978 4.15568404%2C6.28046793 4.41106077%2C6.89147458 C4.42076638%2C6.9146991 4.43136547%2C6.93751648 4.44686993%2C6.97249487 C4.52451483%2C6.76182795 4.5928045%2C6.56208292 4.67130784%2C6.36653318 C4.85550424%2C5.90761881 5.04369502%2C5.45031528 5.2337253%2C4.99382603 C5.24642669%2C4.96329074 5.28714121%2C4.92571021 5.31657339%2C4.92409936 C5.76823475%2C4.89935251 6.2201589%2C4.87995154 6.67206553%2C4.85989561 C6.68814811%2C4.85918754 6.70449349%2C4.86462194 6.73261173%2C4.86906504 C6.7153904%2C4.90517634 6.70314451%2C4.93379985 6.68851602%2C4.96113114 C6.18985076%2C5.89202368 5.69165852%2C6.82314635 5.19059314%2C7.75271126 C5.15814766%2C7.81289676 5.16159894%2C7.85626571 5.19180196%2C7.91436241 C5.71984684%2C8.92962085 6.24580694%2C9.94597679 6.77201231%2C10.9622088 C6.78767444%2C10.9924432 6.80128682%2C11.0238104 6.82390405%2C11.0716756 C6.72954588%2C11.0592668 6.65332003%2C11.0481148 6.57679636%2C11.0393702 L6.57679636%2C11.0393702 Z M15.870621%2C1.69321826 C13.7275728%2C1.69321826 11.584507%2C1.69268721 9.44147619%2C1.69505923 C9.35428335%2C1.69516544 9.32721626%2C1.67438374 9.3280747%2C1.58270707 C9.33269975%2C1.09008882 9.33024707%2C0.597399768 9.33024707%2C0.104746115 L9.33024707%2C0.00641364263 C9.3096971%2C0.00216525498 9.30388074%2C-0.000737809918 9.29874763%2C0.000164972457 C8.00224249%2C0.2273298 6.70589502%2C0.455539024 5.40924973%2C0.681995787 C3.64100627%2C0.990835868 1.87262266%2C1.29877317 0.103941226%2C1.604958 C0.0272773993%2C1.61823422 0.000122707477%2C1.64340591 0.000175264945%2C1.72724076 C0.00254035101%2C5.9075303 0.00257538932%2C10.0878198 7.33847247e-08%2C14.2681094 C-5.24840833e-05%2C14.3564758 0.0281183188%2C14.3820193 0.108758994%2C14.3951539 C0.47168583%2C14.4541888 0.833614074%2C14.5195785 1.19587518%2C14.5828618 C2.52706825%2C14.8154256 3.85822628%2C15.0482196 5.18945439%2C15.2806595 C6.3970323%2C15.4914857 7.60464524%2C15.7020995 8.81229322%2C15.9125009 C8.98089758%2C15.9418856 9.14979977%2C15.9695532 9.33024707%2C16 L9.33024707%2C15.892445 L9.33024707%2C14.6445342 C9.33024707%2C14.6238941 9.33194643%2C14.6030947 9.33007188%2C14.582614 C9.32528915%2C14.5303057 9.34604935%2C14.5050278 9.39971053%2C14.5084442 C9.42877481%2C14.5103029 9.45808435%2C14.5084973 9.48727127%2C14.5084973 C11.6157086%2C14.5084796 13.7441634%2C14.508515 15.8725832%2C14.5082141 C15.914927%2C14.5082141 15.9572533%2C14.5043374 16%2C14.5022309 L16%2C1.69321826 L15.870621%2C1.69321826 Z"/%3E%0A%3C/svg%3E%0A')
        }

        .reports-title-settings-icon .menu-popup-item-icon {
            background-image: url('data:image/svg+xml,%3Csvg%20width%3D%2216px%22%20height%3D%2216px%22%20viewBox%3D%220%200%2016%2016%22%20xmlns%3D%22http%3A//www.w3.org/2000/svg%22%3E%3Cpath%20transform%3D%22translate%28-12%2C%20-11%29%22%20fill%3D%22%23535c69%22%20fill-rule%3D%22evenodd%22%20d%3D%22M22.9549333%2C19.6581413%20C22.7131867%2C20.7945707%2021.8195067%2C21.6882747%2020.68312%2C21.9299507%20C18.5165333%2C22.39074%2016.6344267%2C20.508024%2017.0952933%2C18.3418347%20C17.31324%2C17.317524%2018.34252%2C16.2881373%2019.3668%2C16.0701413%20C21.5329733%2C15.6090733%2023.4158133%2C17.4913707%2022.9549333%2C19.6581413%20M27.4145333%2C17.8349653%20L25.8296133%2C17.57054%20L25.8296133%2C17.57054%20C25.7170133%2C17.1101933%2025.554%2C16.6696867%2025.3424133%2C16.2579133%20C25.33352%2C16.2405893%2025.3355867%2C16.219748%2025.3478133%2C16.2045907%20L26.3423333%2C14.9714347%20C26.5581333%2C14.7055427%2026.5483333%2C14.322992%2026.3208267%2C14.0663907%20L25.6904667%2C13.35694%20C25.4619733%2C13.1005947%2025.0830667%2C13.0463867%2024.79404%2C13.228988%20L23.4386133%2C14.08106%20L23.4386133%2C14.08106%20C22.8510133%2C13.67052%2022.18652%2C13.365788%2021.4700133%2C13.1862373%20C21.4510133%2C13.181464%2021.4366%2C13.165956%2021.43336%2C13.1466053%20L21.1735067%2C11.584968%20C21.1178533%2C11.2475667%2020.82588%2C11%2020.4831467%2C11%20L19.5332133%2C11%20C19.1911733%2C11%2018.8982%2C11.2475667%2018.8437867%2C11.584968%20L18.5824933%2C13.147188%20C18.5792533%2C13.1664907%2018.5648667%2C13.181952%2018.5458933%2C13.1867027%20C17.96604%2C13.3320253%2017.4214%2C13.5613827%2016.92248%2C13.861248%20C16.9056667%2C13.8713547%2016.8845467%2C13.8706093%2016.8685733%2C13.8592%20L15.6058133%2C12.957996%20C15.3280133%2C12.7595147%2014.9464267%2C12.7905533%2014.7039867%2C13.0324853%20L14.0328933%2C13.7040747%20C13.79096%2C13.9465187%2013.75992%2C14.3281147%2013.9589333%2C14.6059293%20L14.8622533%2C15.8712173%20C14.87364%2C15.887168%2014.8744267%2C15.9082867%2014.8643467%2C15.9250987%20C14.5674%2C16.420164%2014.3404267%2C16.9615653%2014.1952533%2C17.536148%20C14.1904533%2C17.5551253%2014.175%2C17.5694693%2014.15572%2C17.5726827%20L12.5849333%2C17.8349893%20C12.2480533%2C17.8906173%2012%2C18.1826347%2012%2C18.525392%20L12%2C19.474608%20C12%2C19.8173413%2012.2480533%2C20.1093827%2012.5849333%2C20.1650107%20L14.1556667%2C20.4272947%20C14.1749733%2C20.4305307%2014.1904533%2C20.4448987%2014.1952267%2C20.4638987%20C14.31332%2C20.933%2014.4815333%2C21.3817267%2014.7026667%2C21.7994387%20C14.71188%2C21.816856%2014.7099333%2C21.8380213%2014.6975467%2C21.8533667%20L13.7079333%2C23.0790467%20C13.49288%2C23.344684%2013.5019067%2C23.7274907%2013.7294267%2C23.9838587%20L14.3592933%2C24.693332%20C14.5878%2C24.950144%2014.96692%2C25.0033733%2015.2559733%2C24.8212373%20L16.6037467%2C23.974568%20C16.6202667%2C23.9641827%2016.64132%2C23.9646027%2016.65748%2C23.9755467%20C17.23272%2C24.3642907%2017.88164%2C24.6505573%2018.5771867%2C24.8212373%20L18.5771867%2C24.8212373%20L18.8438133%2C26.4149853%20C18.8982267%2C26.7524093%2019.1911733%2C27%2019.5332133%2C27%20L20.4831467%2C27%20C20.8258933%2C27%2021.1178533%2C26.7524333%2021.1735067%2C26.4150093%20L21.4338%2C24.853068%20C21.43704%2C24.8337413%2021.4514533%2C24.8182573%2021.4704533%2C24.8135067%20C22.0428133%2C24.6701867%2022.5803733%2C24.44488%2023.0745867%2C24.1503013%20C23.0914%2C24.140288%2023.11244%2C24.14108%2023.1283467%2C24.1524427%20L24.44492%2C25.0927653%20C24.72224%2C25.2919693%2025.1040933%2C25.26072%2025.34628%2C25.01802%20L26.0176667%2C24.3466413%20C26.2593067%2C24.1049413%2026.2920267%2C23.7235547%2026.09136%2C23.445228%20L25.15412%2C22.1311347%20C25.1427333%2C22.115184%2025.14196%2C22.094088%2025.15204%2C22.0772987%20C25.4498667%2C21.5805587%2025.6780533%2C21.0381093%2025.8224267%2C20.4609413%20C25.8271733%2C20.4419413%2025.8426533%2C20.427528%2025.86196%2C20.4242907%20L27.4150667%2C20.1649653%20C27.7529467%2C20.1093133%2028%2C19.8172947%2028%2C19.4745613%20L28%2C18.5253453%20C27.9994933%2C18.1826347%2027.75244%2C17.8906173%2027.4145333%2C17.8349653%22/%3E%3C/svg%3E')
        }
    </style>
    <span class="menu-popup-item reports-title-settings-icon" id="loading"><span
                class="menu-popup-item-icon"></span><span class="menu-popup-item-text">Настройки</span></span>
    <span class="menu-popup-item reports-title-excel-icon" onclick="getExcel();"><span
                class="menu-popup-item-icon"></span><span class="menu-popup-item-text">Экспорт Excel</span></span>

    <script type="text/javascript">
        var url = window.location.href;

        function getExcel() {
            param = 'getexcel=Y';
            _url = location.href;
            _url += (_url.split('?')[1] ? '&' : '?') + param;

            window.open(_url, "_blank")
        }

        function getSettings() {

        }
    </script>
<?php

$activeCols = [
    '№',
    'Наименование меморандума, соглашения, проекта',
    'Сроки реализации',
    'Страна',
    'Иностранный партнер',
    'Ответственное мин/вед',
    'Отрасль',
    'Область',
    'Район',
    'Основание реализации',
    'Статус проекта',
    'Механизм и сроки реализации',
    'Ход реализации по механизму',
    'Ход реализации по факту',
    'Заявлено по "Дорожной карте"',
    'Фактически реализуется',
    'Освоение в 2017г.',
    'Освоение в 2018г.',
    'Освоение в 2019г.',
    'Освоение в 2020-2025гг.',
    'Ответственный сотрудник от ГКИ',
];
?>


    <div id="ajax-add-schema">
        <form id="roadmapSettins" action="" method="post">
            <?
            foreach ($activeCols as $kCol => $col):
                ?>
                <label><input name="activeCols[<?= $kCol ?>]" value="1"
                              type="checkbox"<? if ($arResult['VIEWED_COLS'][$kCol] == 1) echo ' checked'; ?>><?= $col ?>
                </label>
            <? endforeach; ?>

        </form>

    </div>


    <style>
        #ajax-add-schema {
            display: none;
            width: 600px;
            min-height: 400px;
        }

        #ajax-add-schema label {
            width: 100%;
            display: block;
        }

        .modalSettingsTitle {
            font: 20px/26px "OpenSans-Light", Helvetica, Arial, sans-serif;
            padding: 10px;
        }

        .popup-window-titlebar {
            height: auto;
            padding: 10px;
        }
    </style>
    <script type="text/javascript">
        <!--
        BX.ready(function () {
            var schema = new BX.PopupWindow("schema", null, {
                content: BX('ajax-add-schema'),//Контейнер
                closeIcon: {right: "20px", top: "10px"},//Иконка закрытия
                titleBar: {
                    content: BX.create("span", {
                        html: 'Настройки',
                        'props': {'className': 'modalSettingsTitle'}
                    })
                },//Название окна
                zIndex: 0,
                offsetLeft: 0,
                offsetTop: 0,
                draggable: {restrict: true},//Окно можно перетаскивать на странице
                // Если потребуется, можно использовать кнопки управления формой
                buttons: [
                    new BX.PopupWindowButton({
                        text: "Сохранить",
                        className: "popup-window-button-accept",
                        events: {
                            click: function () {
                                BX.ajax.submit(BX("roadmapSettins"), function (data) { // отправка данных из формы с id="myForm" в файл из action="..."
                                    BX('ajax-add-schema').innerHTML = data;
                                });
                            }
                        }
                    }),
                    new BX.PopupWindowButton({
                        text: "Закрыть",
                        className: "webform-button-link-cancel",
                        events: {
                            click: function () {
                                this.popupWindow.close();// закрытие окна
                            }
                        }
                    })
                ]
            });
            $('#loading').click(function () {
                // BX.ajax.insertToNode('https://ya.ru', BX('ajax-add-schema'));//ajax-загрузка контента из url, у меня он помещён в "Короткие ссылки" /bitrix/admin/short_uri_admin.php?lang=ru
                //Можно использовать такой адрес /include/schema.php
                schema.show(); //отображение окна
            });
        });
        //-->
    </script>


<? $this->EndViewTarget(); ?>


<?php

$this->SetViewTarget("sidebar", 100); ?>

    <div class="sidebar-block">
        <b class="r2"></b><b class="r1"></b><b class="r0"></b>
        <div class="sidebar-block-inner">
            <div class="filter-block-title report-filter-block-title">Фильтр</div>
            <div class="reports-description-text">

                <div class="filter-block filter-field-date-combobox filter-field-date-combobox-interval">

                    <form action="" method="GET">
                        <input name="set_filter" value="Y" type="hidden">


                        <div id="report-filter-chfilter">

                            <div class="filter-field">
                                <label class="filter-field-title">Статус</label>
                                <select name="filter[0][]" class="filter-dropdown">
                                    <option value="">Не учитывать</option>
                                    <?
                                    $filtered = false;
                                    if (is_array($arResult['PREFILTER']['=STAGE_ID'])) $filtered = true;
                                    $selected = null;
                                    foreach ($arResult['DEAL_STAGE'] as $k => $v):
                                        if ($filtered && in_array($k, $arResult['PREFILTER']['=STAGE_ID'])) {
                                            $selected = ' selected';
                                        }
                                        ?>
                                        <option value="<?= $k ?>"<?= $selected ?>><?= $v['NAME'] ?></option>
                                        <?
                                        $selected = null;
                                    endforeach; ?>
                                </select>
                            </div>

                            <div class="filter-field">
                                <label class="filter-field-title">Основание реализации</label>
                                <select name="filter[1][]" class="filter-dropdown" multiple="multiple">
                                    <?
                                    $filtered = false;
                                    if (is_array($arResult['PREFILTER']['UF_CRM_1529989379347'])) $filtered = true;
                                    $selected = null;
                                    foreach ($arResult['CRM_DEAL_FIELDS']['UF_CRM_1529989379347']['VALUE'] as $k => $v):
                                        if ($filtered && in_array($k, $arResult['PREFILTER']['UF_CRM_1529989379347'])) {
                                            $selected = ' selected';
                                        }
                                        ?>
                                        <option value="<?= $k ?>"<?= $selected ?>><?= $v ?></option>
                                        <?
                                        $selected = null;
                                    endforeach; ?>
                                </select>
                            </div>


                            <div class="filter-field">
                                <label for="" class="filter-field-title">Ответственное мин/вед</label>
                                <select name="filter[2][]" class="filter-dropdown" multiple="multiple">
                                    <?
                                    $filtered = false;
                                    if (is_array($arResult['PREFILTER']['UF_CRM_1530258582'])) $filtered = true;
                                    $selected = null;
                                    foreach ($arResult['CRM_DEAL_FIELDS']['UF_CRM_1530258582']['VALUE'] as $k => $v):
                                        if ($filtered && in_array($k, $arResult['PREFILTER']['UF_CRM_1530258582'])) {
                                            $selected = ' selected';
                                        }
                                        ?>
                                        <option value="<?= $k ?>"<?= $selected ?>><?= $v ?></option>
                                        <?
                                        $selected = null;
                                    endforeach; ?>
                                </select>
                            </div>

                            <div class="filter-field">
                                <label for="" class="filter-field-title">Страна</label>
                                <select name="filter[3][]" class="filter-dropdown" multiple="multiple">
                                    <?
                                    $filtered = false;
                                    if (is_array($arResult['PREFILTER']['UF_CRM_1529991824'])) $filtered = true;
                                    $selected = null;
                                    foreach ($arResult['CRM_DEAL_FIELDS']['UF_CRM_1529991824']['VALUE'] as $k => $v):
                                        if ($filtered && in_array($k, $arResult['PREFILTER']['UF_CRM_1529991824'])) {
                                            $selected = ' selected';
                                        }
                                        ?>
                                        <option value="<?= $k ?>"<?= $selected ?>><?= $v ?></option>
                                        <?
                                        $selected = null;
                                    endforeach; ?>
                                </select>
                            </div>
                            <div class="filter-field">
                                <label for="" class="filter-field-title">Отрасль</label>
                                <select name="filter[4][]" class="filter-dropdown" multiple="multiple">
                                    <?
                                    $filtered = false;
                                    if (is_array($arResult['PREFILTER']['UF_CRM_1529910635'])) $filtered = true;
                                    $selected = null;
                                    foreach ($arResult['CRM_DEAL_FIELDS']['UF_CRM_1529910635']['VALUE'] as $k => $v):
                                        if ($filtered && in_array($k, $arResult['PREFILTER']['UF_CRM_1529910635'])) {
                                            $selected = ' selected';
                                        }
                                        ?>
                                        <option value="<?= $k ?>"<?= $selected ?>><?= $v ?></option>
                                        <?
                                        $selected = null;
                                    endforeach; ?>
                                </select>
                            </div>
                            <div class="filter-field">
                                <label for="" class="filter-field-title">Область</label>
                                <select name="filter[5][]" class="filter-dropdown" multiple="multiple">
                                    <?
                                    $filtered = false;
                                    if (is_array($arResult['PREFILTER']['UF_CRM_1529911316'])) $filtered = true;
                                    $selected = null;
                                    foreach ($arResult['CRM_DEAL_FIELDS']['UF_CRM_1529911316']['VALUE'] as $k => $v):
                                        if ($filtered && in_array($k, $arResult['PREFILTER']['UF_CRM_1529911316'])) {
                                            $selected = ' selected';
                                        }
                                        ?>
                                        <option value="<?= $k ?>"<?= $selected ?>><?= $v ?></option>
                                        <?
                                        $selected = null;
                                    endforeach; ?>
                                </select>
                            </div>


                            <div class="filter-field chfilter-field">
                                <label class="filter-field-title">Ход реализации</label>
                                <select name="filter[6]" class="filter-dropdown">
                                    <option value="">Не учитывать</option>
                                    <option value="1"<? if ($arResult['get_filter'][6] == 1) echo ' selected'; ?>>по
                                        механизму
                                    </option>
                                    <option value="2"<? if ($arResult['get_filter'][6] == 2) echo ' selected'; ?>>по
                                        факту
                                    </option>
                                </select>
                            </div>

                            <div class="filter-field chfilter-field">
                                <label for="" class="filter-field-title">Иностранные инвестиции</label>
                                <label><input name="filter[19]" value="1"
                                              type="checkbox"<? if ($arResult['get_filter'][19] == 1) echo ' checked'; ?>>
                                    ПИИ</label>
                                <label><input name="filter[20]" value="1"
                                              type="checkbox"<? if ($arResult['get_filter'][20] == 1) echo ' checked'; ?>>
                                    ГЧП</label>
                                <label><input name="filter[21]" value="1"
                                              type="checkbox"<? if ($arResult['get_filter'][21] == 1) echo ' checked'; ?>>
                                    Гранты</label>
                            </div>

                            <div class="filter-field">
                                <label for="" class="filter-field-title">Получение вопросника от инвестора</label>
                                <select name="filter[7]" class="filter-dropdown">
                                    <option value="">Не учитывать</option>
                                    <? foreach ($arResult['CRM_DEAL_FIELDS']['UF_CRM_1530168056178']['VALUE'] as $k => $v): ?>
                                        <option value="<?= $k ?>"<? if ($arResult['get_filter'][7] == $k) echo ' selected'; ?>><?= $v ?></option>
                                    <? endforeach; ?>
                                </select>
                            </div>

                            <div class="filter-field">
                                <label for="" class="filter-field-title">Предоставить инвестору запрашиваемую
                                    информацию</label>
                                <select name="filter[8]" class="filter-dropdown">
                                    <option value="">Не учитывать</option>
                                    <? foreach ($arResult['CRM_DEAL_FIELDS']['UF_CRM_1530190165658']['VALUE'] as $k => $v): ?>
                                        <option value="<?= $k ?>"<? if ($arResult['get_filter'][8] == $k) echo ' selected'; ?>><?= $v ?></option>
                                    <? endforeach; ?>
                                </select>
                            </div>

                            <div class="filter-field">
                                <label for="" class="filter-field-title">Организовация визита в Республику
                                    Узбекистан</label>
                                <select name="filter[9]" class="filter-dropdown">
                                    <option value="">Не учитывать</option>
                                    <? foreach ($arResult['CRM_DEAL_FIELDS']['UF_CRM_1530190410989']['VALUE'] as $k => $v): ?>
                                        <option value="<?= $k ?>"<? if ($arResult['get_filter'][9] == $k) echo ' selected'; ?>><?= $v ?></option>
                                    <? endforeach; ?>
                                </select>
                            </div>

                            <div class="filter-field">
                                <label for="" class="filter-field-title">Подписание соглашения меморандума</label>
                                <select name="filter[10]" class="filter-dropdown">
                                    <option value="">Не учитывать</option>
                                    <? foreach ($arResult['CRM_DEAL_FIELDS']['UF_CRM_1530190499022']['VALUE'] as $k => $v): ?>
                                        <option value="<?= $k ?>"<? if ($arResult['get_filter'][10] == $k) echo ' selected'; ?>><?= $v ?></option>
                                    <? endforeach; ?>
                                </select>
                            </div>

                            <div class="filter-field">
                                <label for="" class="filter-field-title">Подготовка бизнес плана</label>
                                <select name="filter[11]" class="filter-dropdown">
                                    <option value="">Не учитывать</option>
                                    <? foreach ($arResult['CRM_DEAL_FIELDS']['UF_CRM_1530190555857']['VALUE'] as $k => $v): ?>
                                        <option value="<?= $k ?>"<? if ($arResult['get_filter'][11] == $k) echo ' selected'; ?>><?= $v ?></option>
                                    <? endforeach; ?>
                                </select>
                            </div>

                            <div class="filter-field">
                                <label for="" class="filter-field-title">Создание СП/ИП/ГЧП</label>
                                <select name="filter[12]" class="filter-dropdown">
                                    <option value="">Не учитывать</option>
                                    <? foreach ($arResult['CRM_DEAL_FIELDS']['UF_CRM_1530245917416']['VALUE'] as $k => $v): ?>
                                        <option value="<?= $k ?>"<? if ($arResult['get_filter'][12] == $k) echo ' selected'; ?>><?= $v ?></option>
                                    <? endforeach; ?>
                                </select>
                            </div>

                            <div class="filter-field">
                                <label for="" class="filter-field-title">Выделение участка</label>
                                <select name="filter[13]" class="filter-dropdown">
                                    <option value="">Не учитывать</option>
                                    <? foreach ($arResult['CRM_DEAL_FIELDS']['UF_CRM_1530245984072']['VALUE'] as $k => $v): ?>
                                        <option value="<?= $k ?>"<? if ($arResult['get_filter'][13] == $k) echo ' selected'; ?>><?= $v ?></option>
                                    <? endforeach; ?>
                                </select>
                            </div>

                            <div class="filter-field">
                                <label for="" class="filter-field-title">Начало строительных работ</label>
                                <select name="filter[14]" class="filter-dropdown">
                                    <option value="">Не учитывать</option>
                                    <? foreach ($arResult['CRM_DEAL_FIELDS']['UF_CRM_1530246082319']['VALUE'] as $k => $v): ?>
                                        <option value="<?= $k ?>"<? if ($arResult['get_filter'][14] == $k) echo ' selected'; ?>><?= $v ?></option>
                                    <? endforeach; ?>
                                </select>
                            </div>

                            <div class="filter-field">
                                <label for="" class="filter-field-title">Подключение к инфраструктуре</label>
                                <select name="filter[15]" class="filter-dropdown">
                                    <option value="">Не учитывать</option>
                                    <? foreach ($arResult['CRM_DEAL_FIELDS']['UF_CRM_1530246156008']['VALUE'] as $k => $v): ?>
                                        <option value="<?= $k ?>"<? if ($arResult['get_filter'][15] == $k) echo ' selected'; ?>><?= $v ?></option>
                                    <? endforeach; ?>
                                </select>
                            </div>

                            <div class="filter-field">
                                <label for="" class="filter-field-title">Заключение контракта на поставку
                                    оборудования</label>
                                <select name="filter[16]" class="filter-dropdown">
                                    <option value="">Не учитывать</option>
                                    <? foreach ($arResult['CRM_DEAL_FIELDS']['UF_CRM_1530246244794']['VALUE'] as $k => $v): ?>
                                        <option value="<?= $k ?>"<? if ($arResult['get_filter'][16] == $k) echo ' selected'; ?>><?= $v ?></option>
                                    <? endforeach; ?>
                                </select>
                            </div>

                            <div class="filter-field">
                                <label for="" class="filter-field-title">Поставка и монтаж оборудования</label>
                                <select name="filter[17]" class="filter-dropdown">
                                    <option value="">Не учитывать</option>
                                    <? foreach ($arResult['CRM_DEAL_FIELDS']['UF_CRM_1530246340671']['VALUE'] as $k => $v): ?>
                                        <option value="<?= $k ?>"<? if ($arResult['get_filter'][17] == $k) echo ' selected'; ?>><?= $v ?></option>
                                    <? endforeach; ?>
                                </select>
                            </div>

                            <div class="filter-field">
                                <label for="" class="filter-field-title">Ввод в эксплуатацию</label>
                                <select name="filter[18]" class="filter-dropdown">
                                    <option value="">Не учитывать</option>
                                    <? foreach ($arResult['CRM_DEAL_FIELDS']['UF_CRM_1530246434209']['VALUE'] as $k => $v): ?>
                                        <option value="<?= $k ?>"<? if ($arResult['get_filter'][18] == $k) echo ' selected'; ?>><?= $v ?></option>
                                    <? endforeach; ?>
                                </select>
                            </div>


                            <div class="filter-field chfilter-field">
                                <label for="" class="filter-field-title">Освоение</label>
                                <label><input name="filter[22]" value="1"
                                              type="checkbox"<? if ($arResult['get_filter'][22] == 1) echo ' checked'; ?>>
                                    I квартал</label>
                                <label><input name="filter[23]" value="1"
                                              type="checkbox"<? if ($arResult['get_filter'][23] == 1) echo ' checked'; ?>>
                                    II квартал</label><br/>
                                <label><input name="filter[24]" value="1"
                                              type="checkbox"<? if ($arResult['get_filter'][24] == 1) echo ' checked'; ?>>
                                    III квартал</label>
                                <label><input name="filter[25]" value="1"
                                              type="checkbox"<? if ($arResult['get_filter'][25] == 1) echo ' checked'; ?>>
                                    IV квартал</label>
                            </div>

                            <? /*








                            <div class="filter-field filter-field-eventType chfilter-field-STAGE_ID" >
                                <label for="filter_0_4" class="filter-field-title">Статус</label>
                                <select id="filter_0_4" name="filter[4]" class="filter-dropdown">
                                    <option value="">Не учитывать</option><option value="1">Перспективные</option><option value="NEW">Не реализуется</option><option value="PREPARATION">Реализуется</option><option value="PREPAYMENT_INVOICE">Реализуется с отставанием</option><option value="EXECUTING">Реализовано</option><option value="WON">Сделка успешна</option><option value="LOSE">Сделка провалена</option><option value="APOLOGY">Анализ причины провала</option></select>
                            </div>


                            <div class="filter-field chfilter-field-string">
                                <label for="" class="filter-field-title">Ответственный сотрудник</label>
                                <input value="" name="filter[12]" class="filter-textbox" id="" type="text">
                            </div>
*/ ?>


                        </div>


                        <div class="filter-field-buttons">
                            <input id="report-rewrite-filter-button" value="Применить" class="filter-submit"
                                   type="submit">&nbsp;&nbsp;<a href="/roadmap/" class="filter-submit">Отмена</a>
                        </div>
                    </form>


                </div>


            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.filter-dropdown').select2();
        });
    </script>

<?php


$this->EndViewTarget();
?>