<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

if (!CModule::IncludeModule('crm')) {
    ShowError(GetMessage('CRM_MODULE_NOT_INSTALLED'));
    return;
}

$isBizProcInstalled = IsModuleInstalled('bizproc');
if ($isBizProcInstalled) {
    if (!CModule::IncludeModule('bizproc')) {
        ShowError(GetMessage('BIZPROC_MODULE_NOT_INSTALLED'));
        return;
    } elseif (!CBPRuntime::isFeatureEnabled())
        $isBizProcInstalled = false;
}

if (!CAllCrmInvoice::installExternalEntities())
    return;
if (!CCrmQuote::LocalComponentCausedUpdater())
    return;

if (!CModule::IncludeModule('currency')) {
    ShowError(GetMessage('CRM_MODULE_NOT_INSTALLED_CURRENCY'));
    return;
}
if (!CModule::IncludeModule('catalog')) {
    ShowError(GetMessage('CRM_MODULE_NOT_INSTALLED_CATALOG'));
    return;
}
if (!CModule::IncludeModule('sale')) {
    ShowError(GetMessage('CRM_MODULE_NOT_INSTALLED_SALE'));
    return;
}

/** @global CMain $APPLICATION */
global $USER_FIELD_MANAGER, $USER, $APPLICATION, $DB;

use Bitrix\Main;
use Bitrix\Crm;
use Bitrix\Crm\Category\DealCategory;
use Bitrix\Crm\Settings\HistorySettings;
use Bitrix\Crm\WebForm\Manager as WebFormManager;
use Bitrix\Crm\Settings\LayoutSettings;

$userPermissions = CCrmPerms::GetCurrentUserPermissions();
if (!CCrmDeal::CheckReadPermission(0, $userPermissions)) {
    ShowError(GetMessage('CRM_PERMISSION_DENIED'));
    return;
}

//$CCrmDeal = new CCrmDeal(false);
//$CCrmBizProc = new CCrmBizProc('DEAL');
//
//$userID = CCrmSecurityHelper::GetCurrentUserID();
//$isAdmin = CCrmPerms::IsAdmin();
//
//$arResult['CURRENT_USER_ID'] = CCrmSecurityHelper::GetCurrentUserID();
//$arResult['PATH_TO_DEAL_LIST'] = $arParams['PATH_TO_DEAL_LIST'] = CrmCheckPath('PATH_TO_DEAL_LIST', $arParams['PATH_TO_DEAL_LIST'], $APPLICATION->GetCurPage());
//$arResult['PATH_TO_DEAL_WIDGET'] = $arParams['PATH_TO_DEAL_WIDGET'] = CrmCheckPath('PATH_TO_DEAL_WIDGET', $arParams['PATH_TO_DEAL_WIDGET'], $APPLICATION->GetCurPage());
//$arResult['PATH_TO_DEAL_KANBAN'] = $arParams['PATH_TO_DEAL_KANBAN'] = CrmCheckPath('PATH_TO_DEAL_KANBAN', $arParams['PATH_TO_DEAL_KANBAN'], $currentPage);
//$arResult['PATH_TO_DEAL_CALENDAR'] = $arParams['PATH_TO_DEAL_CALENDAR'] = CrmCheckPath('PATH_TO_DEAL_CALENDAR', $arParams['PATH_TO_DEAL_CALENDAR'], $currentPage);
//$arParams['PATH_TO_DEAL_CATEGORY'] = CrmCheckPath('PATH_TO_DEAL_CATEGORY', $arParams['PATH_TO_DEAL_CATEGORY'], $APPLICATION->GetCurPage().'?category_id=#category_id#');
//$arParams['IS_RECURRING'] = isset($arParams['IS_RECURRING']) ? $arParams['IS_RECURRING'] : 'N';
//
//if ($arParams['IS_RECURRING'] == 'Y')
//{
//	$arParams['PATH_TO_DEAL_CATEGORY'] = CrmCheckPath('PATH_TO_DEAL_RECUR_CATEGORY', $arParams['PATH_TO_DEAL_RECUR_CATEGORY'], $APPLICATION->GetCurPage().'?category_id=#category_id#');
//}
//$arParams['PATH_TO_DEAL_WIDGETCATEGORY'] = CrmCheckPath('PATH_TO_DEAL_WIDGETCATEGORY', $arParams['PATH_TO_DEAL_WIDGETCATEGORY'], $APPLICATION->GetCurPage().'?category_id=#category_id#');
//$arParams['PATH_TO_DEAL_KANBANCATEGORY'] = CrmCheckPath('PATH_TO_DEAL_KANBANCATEGORY', $arParams['PATH_TO_DEAL_KANBANCATEGORY'], $APPLICATION->GetCurPage().'?category_id=#category_id#');//!!!
//$arParams['PATH_TO_DEAL_CALENDARCATEGORY'] = CrmCheckPath('PATH_TO_DEAL_CALENDARCATEGORY', $arParams['PATH_TO_DEAL_CALENDARCATEGORY'], $APPLICATION->GetCurPage().'?category_id=#category_id#');
//
//$arParams['PATH_TO_DEAL_DETAILS'] = CrmCheckPath('PATH_TO_DEAL_DETAILS', $arParams['PATH_TO_DEAL_DETAILS'], $APPLICATION->GetCurPage().'?deal_id=#deal_id#&details');
//$arParams['PATH_TO_DEAL_SHOW'] = CrmCheckPath('PATH_TO_DEAL_SHOW', $arParams['PATH_TO_DEAL_SHOW'], $APPLICATION->GetCurPage().'?deal_id=#deal_id#&show');
//$arParams['PATH_TO_DEAL_EDIT'] = CrmCheckPath('PATH_TO_DEAL_EDIT', $arParams['PATH_TO_DEAL_EDIT'], $APPLICATION->GetCurPage().'?deal_id=#deal_id#&edit');
//$arParams['PATH_TO_QUOTE_EDIT'] = CrmCheckPath('PATH_TO_QUOTE_EDIT', $arParams['PATH_TO_QUOTE_EDIT'], $APPLICATION->GetCurPage().'?quote_id=#quote_id#&edit');
//$arParams['PATH_TO_INVOICE_EDIT'] = CrmCheckPath('PATH_TO_INVOICE_EDIT', $arParams['PATH_TO_INVOICE_EDIT'], $APPLICATION->GetCurPage().'?invoice_id=#invoice_id#&edit');
//$arParams['PATH_TO_COMPANY_SHOW'] = CrmCheckPath('PATH_TO_COMPANY_SHOW', $arParams['PATH_TO_COMPANY_SHOW'], $APPLICATION->GetCurPage().'?company_id=#company_id#&show');
//$arParams['PATH_TO_CONTACT_SHOW'] = CrmCheckPath('PATH_TO_CONTACT_SHOW', $arParams['PATH_TO_CONTACT_SHOW'], $APPLICATION->GetCurPage().'?contact_id=#contact_id#&show');
//$arParams['PATH_TO_USER_PROFILE'] = CrmCheckPath('PATH_TO_USER_PROFILE', $arParams['PATH_TO_USER_PROFILE'], '/company/personal/user/#user_id#/');
//$arParams['PATH_TO_USER_BP'] = CrmCheckPath('PATH_TO_USER_BP', $arParams['PATH_TO_USER_BP'], '/company/personal/bizproc/');
//$arParams['NAME_TEMPLATE'] = empty($arParams['NAME_TEMPLATE']) ? CSite::GetNameFormat(false) : str_replace(array("#NOBR#","#/NOBR#"), array("",""), $arParams["NAME_TEMPLATE"]);
//$arResult['PATH_TO_CURRENT_LIST'] = ($arParams['IS_RECURRING'] !== 'Y') ? $arParams['PATH_TO_DEAL_LIST'] : $arParams['PATH_TO_DEAL_RECUR'];
//$arParams['ADD_EVENT_NAME'] = isset($arParams['ADD_EVENT_NAME']) ? $arParams['ADD_EVENT_NAME'] : '';
//$arResult['ADD_EVENT_NAME'] = $arParams['ADD_EVENT_NAME'] !== ''
//	? preg_replace('/[^a-zA-Z0-9_]/', '', $arParams['ADD_EVENT_NAME']) : '';
//
//$arResult['IS_AJAX_CALL'] = isset($_REQUEST['AJAX_CALL']) || isset($_REQUEST['ajax_request']) || !!CAjax::GetSession();
//$arResult['SESSION_ID'] = bitrix_sessid();
//$arResult['NAVIGATION_CONTEXT_ID'] = isset($arParams['NAVIGATION_CONTEXT_ID']) ? $arParams['NAVIGATION_CONTEXT_ID'] : '';
//$arResult['PRESERVE_HISTORY'] = isset($arParams['PRESERVE_HISTORY']) ? $arParams['PRESERVE_HISTORY'] : false;
//
//$arResult['HAVE_CUSTOM_CATEGORIES'] = DealCategory::isCustomized();
//
//$arResult['CATEGORY_ACCESS'] = array(
//	'CREATE' => \CCrmDeal::GetPermittedToCreateCategoryIDs($userPermissions),
//	'READ' => \CCrmDeal::GetPermittedToReadCategoryIDs($userPermissions),
//	'UPDATE' => \CCrmDeal::GetPermittedToUpdateCategoryIDs($userPermissions)
//);
//$arResult['CATEGORY_ID'] = isset($arParams['CATEGORY_ID']) ? (int)$arParams['CATEGORY_ID'] : -1;
//$arResult['ENABLE_SLIDER'] = \Bitrix\Crm\Settings\LayoutSettings::getCurrent()->isSliderEnabled();
//
//$arResult['ENTITY_CREATE_URLS'] = array(
//	\CCrmOwnerType::DealName =>
//		\CCrmOwnerType::GetEntityEditPath(\CCrmOwnerType::Deal, 0, false),
//	\CCrmOwnerType::LeadName =>
//		\CCrmOwnerType::GetEntityEditPath(\CCrmOwnerType::Lead, 0, false),
//	\CCrmOwnerType::CompanyName =>
//		\CCrmOwnerType::GetEntityEditPath(\CCrmOwnerType::Company, 0, false),
//	\CCrmOwnerType::ContactName =>
//		\CCrmOwnerType::GetEntityEditPath(\CCrmOwnerType::Contact, 0, false),
//	\CCrmOwnerType::QuoteName =>
//		CComponentEngine::MakePathFromTemplate($arResult['PATH_TO_QUOTE_EDIT'], array('quote_id' => 0)),
//	\CCrmOwnerType::InvoiceName =>
//		CComponentEngine::MakePathFromTemplate($arResult['PATH_TO_INVOICE_EDIT'], array('invoice_id' => 0))
//);
//


$method = $_SERVER['REQUEST_METHOD'];

if ($method === 'POST') {
    if (isset($_POST['save']) && $_POST['save'] == 'Y') {
        $cols = $_POST['activeCols'];
        CUserOptions::SetOption('roadmap', 'view_col', $_POST['activeCols']);
    }
}


$get_filter = &$_GET['filter'];


$wp_filterFields = [
    0 => '=STAGE_ID',
    1 => 'UF_CRM_1529989379347', //Основание реализации
    2 => 'UF_CRM_1530258582', // Ответственное мин/вед
    3 => 'UF_CRM_1529991824', // Страна
    4 => 'UF_CRM_1529910635', // Отрасль
    5 => 'UF_CRM_1529911316', // Область
    6 => '', // Ход реализации
    61 => 'UF_CRM_1529645950215', //  Ход реализации по механизму
    62 => 'UF_CRM_1529645959248', // Ход реализации по факту
    7 => 'UF_CRM_1530168056178', // Получение вопросника от инвестора
    8 => 'UF_CRM_1530190165658', // Предоставить инвестору запрашиваемую информацию
    9 => 'UF_CRM_1530190410989', // Организовация визита в Республику Узбекистан
    10 => 'UF_CRM_1530190499022', // Подписание соглашения меморандума
    11 => 'UF_CRM_1530190555857', // Подготовка бизнес плана
    12 => 'UF_CRM_1530245917416', // Создание СП/ИП/ГЧП
    13 => 'UF_CRM_1530245984072', // Выделение участка
    14 => 'UF_CRM_1530246082319', // Начало строительных работ
    15 => 'UF_CRM_1530246156008', // Подключение к инфраструктуре
    16 => 'UF_CRM_1530246244794', // Заключение контракта на поставку оборудования
    17 => 'UF_CRM_1530246340671', // Поставка и монтаж оборудования
    18 => 'UF_CRM_1530246434209', // Ввод в эксплуатацию

    19 => 'UF_CRM_1529657664226', // ПИИ
    20 => 'UF_CRM_1529657688691', // ГЧП
    21 => 'UF_CRM_1529657705660', // Гранты

    22 => 'UF_CRM_1530603663343', // Освоение I квартал
    23 => 'UF_CRM_1530603688894', // Освоение II квартал
    24 => 'UF_CRM_1530603706770', // Освоение III квартал
    25 => 'UF_CRM_1530603719820', // Освоение IV квартал
];


$wp_filter = [];
if (is_array($get_filter)) {
    foreach ($get_filter as $k => $v) {
        if ($k == 6) {
            $v = (int)$v;

            if ($v > 0) {
                if ($v == 1) {
                    $k = 61;
                } else {
                    $k = 62;
                }
                $fp = [
                    '!' . $wp_filterFields[$k] => false
                ];
                $wp_filter[] = $fp;
            }
        } else if (in_array($k, [19, 20, 21])) {
            $fp = [
                '!' . $wp_filterFields[$k] => '0|USD'
            ];
            $wp_filter[] = $fp;

        } else if (in_array($k, [22, 23, 24, 25])) {
            $fp = [
                '!' . $wp_filterFields[$k] => '0|USD'
            ];
            $wp_filter[] = $fp;
        } else if ($v[0] != '') {
            $wp_filter[$wp_filterFields[$k]] = $v;
        }
    }
}

$arResult['PREFILTER'] = $wp_filter;
$arResult['get_filter'] = $get_filter;
if (count($wp_filter)) {
    $wp_filter['LOGIC'] = 'AND';
}


$items = CCrmDeal::GetList([], $wp_filter);
while ($item = $items->GetNext()) {
    $arResult['ITEMS'][] = $item;
}


$sql = 'SELECT 
  `b_user_field`.`ID`,
  `b_user_field_enum`.`ID` as ENUM_ID,
  `b_user_field`.`FIELD_NAME`,
  `b_user_field_lang`.`EDIT_FORM_LABEL`,
  `b_user_field_lang`.`LIST_COLUMN_LABEL`,
  `b_user_field_lang`.`LIST_FILTER_LABEL`,
  `b_user_field_enum`.`VALUE`,
  `b_user_field_enum`.`SORT`
FROM
  `b_user_field`
  INNER JOIN `b_user_field_lang` ON (`b_user_field`.`ID` = `b_user_field_lang`.`USER_FIELD_ID`)
  LEFT OUTER JOIN `b_user_field_enum` ON (`b_user_field`.`ID` = `b_user_field_enum`.`USER_FIELD_ID`)
WHERE
  `b_user_field`.`ENTITY_ID` = \'CRM_DEAL\' AND 
  `b_user_field_lang`.`LANGUAGE_ID` = \'ru\'
ORDER BY
  `b_user_field_enum`.`SORT`';


global $DB;
$res = $DB->Query($sql);

$enumsVal = [];
while ($row = $res->fetch()) {
    $enumsVal[$row['FIELD_NAME']]['ID'] = $row['ID'];
    $enumsVal[$row['FIELD_NAME']]['EDIT_FORM_LABEL'] = $row['EDIT_FORM_LABEL'];
    $enumsVal[$row['FIELD_NAME']]['LIST_COLUMN_LABEL'] = $row['LIST_COLUMN_LABEL'];
    $enumsVal[$row['FIELD_NAME']]['LIST_FILTER_LABEL'] = $row['LIST_FILTER_LABEL'];
    if ($row['VALUE']) {
        $enumsVal[$row['FIELD_NAME']]['VALUE'][$row['ENUM_ID']] = $row['VALUE'];
    }
}
$arResult['CRM_DEAL_FIELDS'] = $enumsVal;

$arResult['VIEWED_COLS'] = CUserOptions::GetOption('roadmap', 'view_col', [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]);

$arResult['DEAL_STAGE'] = CCrmStatus::GetStatus('DEAL_STAGE');

if (isset($_GET['getexcel']) && $_GET['getexcel'] == 'Y') {
    $this->IncludeComponentTemplate('excel');
} else {
    $this->IncludeComponentTemplate();
}


?>