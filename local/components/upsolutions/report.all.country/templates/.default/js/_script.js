$(function(){
	console.log(data);
    var salesPivotGrid = $("#sales").dxPivotGrid({
        allowSortingBySummary: true,
        allowSorting: true,
        allowFiltering: true,
        showBorders: true,
        "export": {
            enabled: true,
            fileName: "Тестовый"
        },
        dataSource: {
            fields: [
              {
                caption: "Федеральный округ",
                width: 120,
                dataField: "fo",
                area: "row",
                headerFilter: {
                    allowSearch: true
                }
            }, {
                caption: "Субъект РФ",
                dataField: "s_rf",
                width: 150,
                area: "row",
                headerFilter: {
                    allowSearch: true
                }
            }, {
                caption: "Группа",
                dataField: "grp",
                width: 150,
                area: "row",
                headerFilter: {
                    allowSearch: true
                }
            },{
                caption: "Год",
                dataField: "year",
                width: 150,
                area: "row",
                headerFilter: {
                    allowSearch: true
                }
            }, {
                caption: "Производство",
                dataField: "p1",
                dataType: "number",
                summaryType: "sum",
                format: "#0,000.00",
                area: "data"
            }, {
                caption: "Производственная мощность",
                dataField: "p2",
                dataType: "number",
                summaryType: "min",
                format: "#0,000.00",
                area: "data"
            }, {
                caption: "Регион взаимной торговли (вывоз)",
                dataField: "p3",
                dataType: "number",
                summaryType: "sum",
                format: "#0,000.00",
                area: "data"
            }, {
                caption: "Внешняя торговля (экспорт)",
                dataField: "p4",
                dataType: "number",
                summaryType: "sum",
                format: "#0,000.00",
                area: "data"
            }, {
                caption: "Регион взаимной торговли (ввоз)",
                dataField: "p5",
                dataType: "number",
                summaryType: "sum",
                format: "#0,000.00",
                area: "data"
            }, {
                caption: "Внешняя торговля (импорт)",
                dataField: "p6",
                dataType: "number",
                summaryType: "sum",
                format: "#0,000.00",
                area: "data"
            }, {
                caption: "Потребление",
                dataField: "p7",
                dataType: "number",
                summaryType: "sum",
                format: "#0,000.00",
                area: "data"
            }, {
                caption: "Прогнозное потребление",
                dataField: "p8",
                dataType: "number",
                summaryType: "sum",
                format: "#0,000.00",
                area: "data"
            }
            ],
            store: d_data,
        },
        fieldChooser: {
            enabled: false
        }
    }).dxPivotGrid("instance");

    var salesFieldChooser = $("#sales-fieldchooser").dxPivotGridFieldChooser({
        dataSource: salesPivotGrid.getDataSource(),
        texts: {
            allFields: "All",
            columnFields: "Columns",
            dataFields: "Data",
            rowFields: "Rows",
            filterFields: "Filter"
        },
        width: 400,
        height: 400
    }).dxPivotGridFieldChooser("instance");

    var applyButton = $("#applyButton").dxButton({
        text: "Apply",
        visible: false,
        type: "default",
        onClick: function(data) {
            salesFieldChooser.applyChanges();
        }
    }).dxButton("instance");

    var cancelButton = $("#cancelButton").dxButton({
        text: "Cancel",
        visible: false,
        onClick: function(data) {
            salesFieldChooser.cancelChanges();
        }
    }).dxButton("instance");
});