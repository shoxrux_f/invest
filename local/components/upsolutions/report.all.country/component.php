<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

if (!CModule::IncludeModule('crm')) {
    ShowError(GetMessage('CRM_MODULE_NOT_INSTALLED'));
    return;
}

$COUNTRY_UF = 'UF_CRM_1529991824';

function UpsendJsonAnswer($result)
{
    global $APPLICATION;

    $APPLICATION->RestartBuffer();
    header('Content-Type: application/json');

    echo \Bitrix\Main\Web\Json::encode($result);

    CMain::FinalActions();
    die();
}

// Статусы
// $dbStatus = \CCrmStatus::GetList(['SORT' => 'ASC'],['ENTITY_ID'=>'DEAL_STAGE']);
// while ($rsStatus = $dbStatus->fetch())
// {
//     $arStatus[$rsStatus['STATUS_ID']] = $rsStatus;
// }

// Пользовательские свойства
global $DB;
$rsField = CUserTypeEntity::GetList( array($by=>$order), array("ENTITY_ID"=>"CRM_DEAL") );
while($arField = $rsField->Fetch())
{
    $fieldName = $DB->Query("
        SELECT EDIT_FORM_LABEL AS NAME
        FROM b_user_field_lang
        WHERE USER_FIELD_ID=".$arField['ID']."
    ")->Fetch();
    $arField['NAME'] = $fieldName['NAME'];

    if($arField['USER_TYPE_ID'] == 'enumeration'){
            $rsMultiField = CUserFieldEnum::GetList(array(), array(
                "USER_FIELD_ID" => $arField["ID"],
            ));
            while ($arMultiField = $rsMultiField->GetNext()){
                $arField['LIST'][$arMultiField['ID']] = $arMultiField;
            }
    }
    $arUserField[$arField['FIELD_NAME']] = $arField;
}


// Все сделки
$arCountryReport = [];
$dbDeal = \CCrmDeal::GetListEx(
    [],
    ['CATEGORY_ID'=>0, '!UF_CRM_1529991824'=>NULL],
    false,
    false,
    ['*','UF_*']
);
$arCountryReport = [[
    'Country' => 'Всего',
    'All_projects_count'=> 0,
    'All_projects_sum'=> 0,
    'Real_projects_count'=> 0,
    'Real_projects_sum'=> 0,
    'Process_projects_count'=> 0,
    'Process_projects_sum'=> 0,
    'None_projects_count'=> 0,
    'None_projects_sum'=> 0,
    'Fact_sum'=> 0,
    '2018_wait_sum'=> 0,
    '2019_wait_sum'=> 0,
]];

while ($rsDeal = $dbDeal->Fetch())
{
    //Всего
    $arCountryReport[0]['All_projects_count'] += 1;
    $arCountryReport[0]['All_projects_sum'] += $rsDeal['OPPORTUNITY_ACCOUNT'];
    $arCountryReport[0]['Real_projects_count'] += $rsDeal['STAGE_ID'] == 'EXECUTING' ? 1 : 0;
    $arCountryReport[0]['Real_projects_sum'] += $rsDeal['STAGE_ID'] == 'EXECUTING' ? $rsDeal['OPPORTUNITY_ACCOUNT'] : 0;
    $arCountryReport[0]['Process_projects_count'] += in_array($rsDeal['STAGE_ID'], ['PREPAYMENT_INVOICE','PREPARATION','3']) ? 1 : 0;
    $arCountryReport[0]['Process_projects_sum'] += in_array($rsDeal['STAGE_ID'], ['PREPAYMENT_INVOICE','PREPARATION','3']) ? $rsDeal['OPPORTUNITY_ACCOUNT'] : 0;
    $arCountryReport[0]['None_projects_count'] += $rsDeal['STAGE_ID'] == 'NEW' ? 1 : 0;
    $arCountryReport[0]['None_projects_sum'] += $rsDeal['STAGE_ID'] == 'NEW' ? $rsDeal['OPPORTUNITY_ACCOUNT'] : 0;
    $arCountryReport[0]['Fact_sum'] += ($rsDeal['UF_CRM_1530603763871'] + $rsDeal['UF_CRM_1530603663343'] + $rsDeal['UF_CRM_1530603688894'] + $rsDeal['UF_CRM_1530603706770'] + $rsDeal['UF_CRM_1530603719820']);
    $arCountryReport[0]['2018_wait_sum']  += ($rsDeal['UF_CRM_1530603663343'] + $rsDeal['UF_CRM_1530603688894'] + $rsDeal['UF_CRM_1530603706770'] + $rsDeal['UF_CRM_1530603719820']);
    $arCountryReport[0]['2019_wait_sum'] += $rsDeal['UF_CRM_1530603783840'];

    //По странам
    $arCountryReport[$rsDeal[$COUNTRY_UF]]['Country'] = $arUserField[$COUNTRY_UF]['LIST'][$rsDeal[$COUNTRY_UF]]['VALUE'];
    $arCountryReport[$rsDeal[$COUNTRY_UF]]['All_projects_count'] += 1;
    $arCountryReport[$rsDeal[$COUNTRY_UF]]['All_projects_sum'] += $rsDeal['OPPORTUNITY_ACCOUNT'];
    $arCountryReport[$rsDeal[$COUNTRY_UF]]['Real_projects_count'] += $rsDeal['STAGE_ID'] == 'EXECUTING' ? 1 : 0;
    $arCountryReport[$rsDeal[$COUNTRY_UF]]['Real_projects_sum'] += $rsDeal['STAGE_ID'] == 'EXECUTING' ? $rsDeal['OPPORTUNITY_ACCOUNT'] : 0;
    $arCountryReport[$rsDeal[$COUNTRY_UF]]['Process_projects_count'] += in_array($rsDeal['STAGE_ID'], ['PREPAYMENT_INVOICE','PREPARATION','3']) ? 1 : 0;
    $arCountryReport[$rsDeal[$COUNTRY_UF]]['Process_projects_sum'] += in_array($rsDeal['STAGE_ID'], ['PREPAYMENT_INVOICE','PREPARATION','3']) ? $rsDeal['OPPORTUNITY_ACCOUNT'] : 0;
    $arCountryReport[$rsDeal[$COUNTRY_UF]]['None_projects_count'] += $rsDeal['STAGE_ID'] == 'NEW' ? 1 : 0;
    $arCountryReport[$rsDeal[$COUNTRY_UF]]['None_projects_sum'] += $rsDeal['STAGE_ID'] == 'NEW' ? $rsDeal['OPPORTUNITY_ACCOUNT'] : 0;
    $arCountryReport[$rsDeal[$COUNTRY_UF]]['Fact_sum'] += ($rsDeal['UF_CRM_1530603763871'] + $rsDeal['UF_CRM_1530603663343'] + $rsDeal['UF_CRM_1530603688894'] + $rsDeal['UF_CRM_1530603706770'] + $rsDeal['UF_CRM_1530603719820']);
    $arCountryReport[$rsDeal[$COUNTRY_UF]]['2018_wait_sum']  += ($rsDeal['UF_CRM_1530603663343'] + $rsDeal['UF_CRM_1530603688894'] + $rsDeal['UF_CRM_1530603706770'] + $rsDeal['UF_CRM_1530603719820']);
    $arCountryReport[$rsDeal[$COUNTRY_UF]]['2019_wait_sum'] += $rsDeal['UF_CRM_1530603783840'];
}

$arResult = $arCountryReport;

$this->IncludeComponentTemplate();


?>