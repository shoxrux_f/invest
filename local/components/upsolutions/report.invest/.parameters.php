<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arCurrentValues */
/** @global CUserTypeManager $USER_FIELD_MANAGER */

$arComponentParameters = array(
	"PARAMETERS" => array(
		"VARIABLE_ALIASES" => array(
		),
		"SEF_MODE" => array(
			"sections" => array(
				"NAME" => 'Список типов',
				"DEFAULT" => "",
				"VARIABLES" => array(
				),
			),
			"country" => array(
				'NAME' => 'Страны',
				'DEFAULT' => "country/",
			),
			"foundation" => array(
					'NAME' => "Основание реализации",
					'DEFAULT' => "foundation/",
			),
			"industry" => array(
					'NAME' => "Отрасль",
					'DEFAULT' => "industry/",
			),
			"liable" => array(
					'NAME' => "Ответственное мин/вед",
					'DEFAULT' => "liable/",
			),
			"partner" => array(
					'NAME' => "Иностранные партнеры",
					'DEFAULT' => "partner/",
			),
			"region" => array(
				'NAME' => 'Область',
				'DEFAULT' => "region/",
			),
			"roadmap" => array(
				'NAME' => 'Дорожная карта',
				'DEFAULT' => "roadmap/",
			),
		),
	),
);

