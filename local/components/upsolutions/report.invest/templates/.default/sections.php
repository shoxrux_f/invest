<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);
$APPLICATION->SetTitle("Типы отчетов");
$APPLICATION->SetAdditionalCSS($templateFolder.'/css/sections.css');
?>
<div class="up_sections">
	<?foreach ($arResult['MENU'] as $code=>$menu){?>
	<div class="col-sm-4">
    	<a class="element-box el-tablo" href="<?=$arResult['URL_TEMPLATES'][$code]?>">
    		<div class="label"><?=$menu?></div>
    		<div class="value"><?=count($arResult['RESULT'][$code]['COUNT'])?></div>
    		<div class="trending trending-up-basic">
    			<span><?=number_format($arResult['RESULT'][$code]['SUM'], 0,","," ")?>$</span>
    		</div>
    	</a>
	</div>
	<?} ?>
</div>