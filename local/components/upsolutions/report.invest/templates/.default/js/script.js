$(function(){
	var arr = Object.values(b_data);
    $("#sales").dxDataGrid({
        dataSource: arr,
        columnAutoWidth: true,
        allowColumnReordering: true,
        showBorders: true,
        paging: {
            pageSize: 1000
        },
        "export": {
            enabled: true,
            fileName: "Отчёт_по_странам"
        },
        sorting: {
            mode: "none"
        },
        columns: [
                  {
                	  caption: "Страна",
                	  dataField: "Country",
                	  width: 200
                  },
                  {
                	  caption: "Всего проектов",
                	  columns: [{
                		  caption: "кол-во",
                		  dataField: "All_projects_count",
                		  width: 100,
                		  sortOrder: "desc"
                	  },
                	  {
                		  caption: "сумма",
                		  dataField: "All_projects_sum",
                		  width: 150,
                		  format: "fixedPoint"
                	  }]
                  },
                  {
                	  caption: "Реализованные проекты",
                	  columns: [{
                		  caption: "кол-во",
                		  dataField: "Real_projects_count",
                		  width: 100,
                	  },
                	  {
                		  caption: "сумма",
                		  dataField: "Real_projects_sum",
                		  format: "fixedPoint",
                		  width: 120,
                	  }]
                  },
                  {
                	  caption: "На стадии реализации",
                	  columns: [{
                		  caption: "кол-во",
                		  dataField: "Process_projects_count",
                		  width: 100,
                	  },
                	  {
                		  caption: "сумма",
                		  dataField: "Process_projects_sum",
                		  format: "fixedPoint",
                		  width: 120,
                	  }]
                  },
                  {
                	  caption: "Нереализуемые проекты",
                	  columns: [{
                		  caption: "кол-во",
                		  dataField: "None_projects_count",
                		  width: 100,
                	  },
                	  {
                		  caption: "сумма",
                		  dataField: "None_projects_sum",
                		  format: "fixedPoint",
                		  width: 120,
                	  }]
                  },
                  {
                	  caption: "Фактическое освоение",
                	  dataField: "Fact_sum",
                	  format: "fixedPoint",
                  },
                  {
                	  caption: "Ожидаемое освоение по итогам 2018 г.",
                	  dataField: "2018_wait_sum",
                	  format: "fixedPoint",
                  },
                  {
                	  caption: "Ожидаемое освоение по итогам 2019 г.",
              		  dataField: "2019_wait_sum",
              		  format: "fixedPoint",
                  }
     	]
    });


});