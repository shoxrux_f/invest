<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$APPLICATION->SetTitle("Отчет по странам");

$UF_FILTER_TYPE = 'UF_CRM_1529991824'; //Код страны


// Пользовательские свойства
global $DB;
$rsField = CUserTypeEntity::GetList( array($by=>$order), array("ENTITY_ID"=>"CRM_DEAL") );
while($arField = $rsField->Fetch())
{
    $fieldName = $DB->Query("
        SELECT EDIT_FORM_LABEL AS NAME
        FROM b_user_field_lang
        WHERE USER_FIELD_ID=".$arField['ID']."
    ")->Fetch();
    $arField['NAME'] = $fieldName['NAME'];

    if($arField['USER_TYPE_ID'] == 'enumeration'){
        $rsMultiField = CUserFieldEnum::GetList(array(), array(
            "USER_FIELD_ID" => $arField["ID"],
        ));
        while ($arMultiField = $rsMultiField->GetNext()){
            $arField['LIST'][$arMultiField['ID']] = $arMultiField;
        }
    }
    $arUserField[$arField['FIELD_NAME']] = $arField;
}

//Фильтр
$arFilter = [];
$arFilter['CATEGORY_ID'] = 0;
$arFilter['STAGE_ID'] = array('2','3','PREPAYMENT_INVOICE','PREPARATION','EXECUTING','NEW');
$arFilter['!'.$UF_FILTER_TYPE] = NULL;
$dbDeal = \CCrmDeal::GetListEx(
    [],
    $arFilter,
    false,
    false,
    ['*','UF_*']
);

$arCountryReport = [[
    'Country' => 'Всего',
    'All_projects_count'=> 0,
    'All_projects_sum'=> 0,
    'Real_projects_count'=> 0,
    'Real_projects_sum'=> 0,
    'Process_projects_count'=> 0,
    'Process_projects_sum'=> 0,
    'None_projects_count'=> 0,
    'None_projects_sum'=> 0,
    'Fact_sum'=> 0,
    '2018_wait_sum'=> 0,
    '2019_wait_sum'=> 0,
]];

while ($rsDeal = $dbDeal->Fetch())
{
    //Всего
    $arCountryReport[0]['All_projects_count'] += 1;
    $arCountryReport[0]['All_projects_sum'] += $rsDeal['OPPORTUNITY_ACCOUNT'];
    $arCountryReport[0]['Real_projects_count'] += $rsDeal['STAGE_ID'] == 'EXECUTING' ? 1 : 0;
    $arCountryReport[0]['Real_projects_sum'] += $rsDeal['STAGE_ID'] == 'EXECUTING' ? $rsDeal['OPPORTUNITY_ACCOUNT'] : 0;
    $arCountryReport[0]['Process_projects_count'] += in_array($rsDeal['STAGE_ID'], ['PREPAYMENT_INVOICE','PREPARATION','3']) ? 1 : 0;
    $arCountryReport[0]['Process_projects_sum'] += in_array($rsDeal['STAGE_ID'], ['PREPAYMENT_INVOICE','PREPARATION','3']) ? $rsDeal['OPPORTUNITY_ACCOUNT'] : 0;
    $arCountryReport[0]['None_projects_count'] += $rsDeal['STAGE_ID'] == 'NEW' ? 1 : 0;
    $arCountryReport[0]['None_projects_sum'] += $rsDeal['STAGE_ID'] == 'NEW' ? $rsDeal['OPPORTUNITY_ACCOUNT'] : 0;
    $arCountryReport[0]['Fact_sum'] += ($rsDeal['UF_CRM_1530603763871'] + $rsDeal['UF_CRM_1530603663343'] + $rsDeal['UF_CRM_1530603688894'] + $rsDeal['UF_CRM_1530603706770'] + $rsDeal['UF_CRM_1530603719820']);
    $arCountryReport[0]['2018_wait_sum']  += ($rsDeal['UF_CRM_1530603663343'] + $rsDeal['UF_CRM_1530603688894'] + $rsDeal['UF_CRM_1530603706770'] + $rsDeal['UF_CRM_1530603719820']);
    $arCountryReport[0]['2019_wait_sum'] += $rsDeal['UF_CRM_1530603783840'];

    //По странам
    $arCountryReport[$rsDeal[$UF_FILTER_TYPE]]['Country'] = $arUserField[$UF_FILTER_TYPE]['LIST'][$rsDeal[$UF_FILTER_TYPE]]['VALUE'];
    $arCountryReport[$rsDeal[$UF_FILTER_TYPE]]['All_projects_count'] += 1;
    $arCountryReport[$rsDeal[$UF_FILTER_TYPE]]['All_projects_sum'] += $rsDeal['OPPORTUNITY_ACCOUNT'];
    $arCountryReport[$rsDeal[$UF_FILTER_TYPE]]['Real_projects_count'] += $rsDeal['STAGE_ID'] == 'EXECUTING' ? 1 : 0;
    $arCountryReport[$rsDeal[$UF_FILTER_TYPE]]['Real_projects_sum'] += $rsDeal['STAGE_ID'] == 'EXECUTING' ? $rsDeal['OPPORTUNITY_ACCOUNT'] : 0;
    $arCountryReport[$rsDeal[$UF_FILTER_TYPE]]['Process_projects_count'] += in_array($rsDeal['STAGE_ID'], ['PREPAYMENT_INVOICE','PREPARATION','3']) ? 1 : 0;
    $arCountryReport[$rsDeal[$UF_FILTER_TYPE]]['Process_projects_sum'] += in_array($rsDeal['STAGE_ID'], ['PREPAYMENT_INVOICE','PREPARATION','3']) ? $rsDeal['OPPORTUNITY_ACCOUNT'] : 0;
    $arCountryReport[$rsDeal[$UF_FILTER_TYPE]]['None_projects_count'] += $rsDeal['STAGE_ID'] == 'NEW' ? 1 : 0;
    $arCountryReport[$rsDeal[$UF_FILTER_TYPE]]['None_projects_sum'] += $rsDeal['STAGE_ID'] == 'NEW' ? $rsDeal['OPPORTUNITY_ACCOUNT'] : 0;
    $arCountryReport[$rsDeal[$UF_FILTER_TYPE]]['Fact_sum'] += ($rsDeal['UF_CRM_1530603763871'] + $rsDeal['UF_CRM_1530603663343'] + $rsDeal['UF_CRM_1530603688894'] + $rsDeal['UF_CRM_1530603706770'] + $rsDeal['UF_CRM_1530603719820']);
    $arCountryReport[$rsDeal[$UF_FILTER_TYPE]]['2018_wait_sum']  += ($rsDeal['UF_CRM_1530603663343'] + $rsDeal['UF_CRM_1530603688894'] + $rsDeal['UF_CRM_1530603706770'] + $rsDeal['UF_CRM_1530603719820']);
    $arCountryReport[$rsDeal[$UF_FILTER_TYPE]]['2019_wait_sum'] += $rsDeal['UF_CRM_1530603783840'];
}

echo "<pre>";print_r($arCountryReport);echo"</pre>";


?>