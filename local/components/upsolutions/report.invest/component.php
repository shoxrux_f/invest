<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

if (!CModule::IncludeModule('crm')) {
    ShowError(GetMessage('CRM_MODULE_NOT_INSTALLED'));
    return;
}
$arMenu = [
    "country" => "Страны",
    "foundation" => "Основание реализации",
    "industry" => "Отрасль",
    "liable" => "Ответственное мин/вед",
    "partner" => "Иностранные партнеры",
    "region" => "Область"
];

$arDefaultUrlTemplates404 = array(
    "sections" => "",
    "country" => "country/",
    "foundation" => "foundation/",
    "industry" => "industry/",
    "liable" => "liable/",
    "partner" => "partner/",
    "region" => "region/",
    "roadmap" => "roadmap/"
);


$arDefaultVariableAliases404 = array();
$arDefaultVariableAliases = array();
$arComponentVariables = array();

if($arParams["SEF_MODE"] == "Y")
{
	$arVariables = array();

	$engine = new CComponentEngine($this);
	if (\Bitrix\Main\Loader::includeModule('iblock'))
	{
		$engine->addGreedyPart("#SECTION_CODE_PATH#");
		$engine->addGreedyPart("#SMART_FILTER_PATH#");
		$engine->setResolveCallback(array("CIBlockFindTools", "resolveComponentEngine"));
	}
	$arUrlTemplates = CComponentEngine::MakeComponentUrlTemplates($arDefaultUrlTemplates404, $arParams["SEF_URL_TEMPLATES"]);
	$arVariableAliases = CComponentEngine::MakeComponentVariableAliases($arDefaultVariableAliases404, $arParams["VARIABLE_ALIASES"]);

	$componentPage = $engine->guessComponentPath(
		$arParams["SEF_FOLDER"],
		$arUrlTemplates,
		$arVariables
	);

	$b404 = false;
	if(!$componentPage)
	{
		$componentPage = "sections";
		$b404 = true;
	}
	$result = [];
	if($componentPage == "sections"){

	    $dbDeal = \CCrmDeal::GetListEx(
	        [],
	        ['CATEGORY_ID'=>0, 'STAGE_ID'=>array('2','3','PREPAYMENT_INVOICE','PREPARATION','EXECUTING','NEW')],
	        false,
	        false,
	        ['*','UF_*']
	    );

	    while ($rsDeal = $dbDeal->fetch())
        {
            if($rsDeal['UF_CRM_1529991824']){
                $result['country']['COUNT'][$rsDeal['UF_CRM_1529991824']] = 1;
                $result['country']['SUM'] += $rsDeal['OPPORTUNITY_ACCOUNT'];
            }

            if($rsDeal['UF_CRM_1529989379347']){
                $result['foundation']['COUNT'][$rsDeal['UF_CRM_1529989379347']] = 1;
                $result['foundation']['SUM'] += $rsDeal['OPPORTUNITY_ACCOUNT'];
            }

            if($rsDeal['UF_CRM_1529910635']){
                $result['industry']['COUNT'][$rsDeal['UF_CRM_1529910635']] = 1;
                $result['industry']['SUM'] += $rsDeal['OPPORTUNITY_ACCOUNT'];
            }

            if($rsDeal['UF_CRM_1530258582']){
                $result['liable']['COUNT'][$rsDeal['UF_CRM_1530258582']] = 1;
                $result['liable']['SUM'] += $rsDeal['OPPORTUNITY_ACCOUNT'];
            }

            if($rsDeal['COMPANY_ID']){
                $result['partner']['COUNT'][$rsDeal['COMPANY_ID']] = 1;
                $result['partner']['SUM'] += $rsDeal['OPPORTUNITY_ACCOUNT'];
            }

            if($rsDeal['UF_CRM_1529911316']){
                $result['region']['COUNT'][$rsDeal['UF_CRM_1529911316']] = 1;
                $result['region']['SUM'] += $rsDeal['OPPORTUNITY_ACCOUNT'];
            }

        }
}


	if($b404 && CModule::IncludeModule('iblock'))
	{
		$folder404 = str_replace("\\", "/", $arParams["SEF_FOLDER"]);
		if ($folder404 != "/")
			$folder404 = "/".trim($folder404, "/ \t\n\r\0\x0B")."/";
		if (substr($folder404, -1) == "/")
			$folder404 .= "index.php";

		if ($folder404 != $APPLICATION->GetCurPage(true))
		{
			\Bitrix\Iblock\Component\Tools::process404(
				""
				,($arParams["SET_STATUS_404"] === "Y")
				,($arParams["SET_STATUS_404"] === "Y")
				,($arParams["SHOW_404"] === "Y")
				,$arParams["FILE_404"]
			);
		}
	}

	CComponentEngine::InitComponentVariables($componentPage, $arComponentVariables, $arVariableAliases, $arVariables);
	$arResult = array(
		"FOLDER" => $arParams["SEF_FOLDER"],
		"URL_TEMPLATES" => $arUrlTemplates,
		"VARIABLES" => $arVariables,
		"ALIASES" => $arVariableAliases,
	    'MENU'=>$arMenu,
	    'RESULT'=>$result
	);
}
else
{
    ShowError('Включите поддержку ЧПУ');
}

$this->IncludeComponentTemplate($componentPage);


?>