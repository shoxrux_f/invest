<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * Bitrix vars
 * @global CUser $USER
 * @global CMain $APPLICATION
 * @global CDatabase $DB
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponent $component
 */

// Js
Bitrix\Main\Page\Asset::getInstance()->addJs('https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js');
Bitrix\Main\Page\Asset::getInstance()->addJs($this->__component->__template->__folder.'/js/jszip.min.js');
Bitrix\Main\Page\Asset::getInstance()->addJs($this->__component->__template->__folder.'/js/dx.all.js');
Bitrix\Main\Page\Asset::getInstance()->addJs($this->__component->__template->__folder.'/js/script.js');
Bitrix\Main\Page\Asset::getInstance()->addJs($this->__component->__template->__folder.'/js/data.js');

// // Css
Bitrix\Main\Page\Asset::getInstance()->addCss($this->__component->__template->__folder.'/css/dx.common.css');
Bitrix\Main\Page\Asset::getInstance()->addCss($this->__component->__template->__folder.'/css/dx.softblue.css');
Bitrix\Main\Page\Asset::getInstance()->addCss($this->__component->__template->__folder.'/css/style.css');

$APPLICATION->IncludeComponent(
    'bitrix:crm.control_panel',
    'upsolutions',
    array(
        'ID' => 'REPORT_CITY',
        'ACTIVE_ITEM_ID' => 'REPORT_CITY',
        'PATH_TO_REPORT_CITY' => isset($arResult['PATH_TO_REPORT_CITY']) ? $arResult['PATH_TO_REPORT_CITY'] : '',
        'PATH_TO_REPORT_DEPARTMENT' => isset($arResult['PATH_TO_REPORT_DEPARTMENT']) ? $arResult[PATH_TO_REPORT_DEPARTMENT] : '',
        'PATH_TO_CONTACT_LIST' => isset($arResult['PATH_TO_CONTACT_LIST']) ? $arResult['PATH_TO_CONTACT_LIST'] : '',
    ),
    $component
);
?>
<script>
	var	b_data = <?echo \Bitrix\Main\Web\Json::encode($arResult)?>;
</script>
<div class="dx-viewport demo-container">
    <div id="sales"></div>
    <div class="container">
        <div id="sales-fieldchooser"></div>
        <div class="bottom-bar">
            <div id="applyButton"></div>
            <div id="cancelButton"></div>
        </div>
    </div>
</div>