$(function(){
	var arr = Object.values(b_data);
    $("#sales").dxDataGrid({
        dataSource: arr,
        columnAutoWidth: true,
        showBorders: true,
        paging: {
            pageSize: 1000
        },
        "export": {
            enabled: true,
            fileName: "Отчёт"
        },
        sorting: {
            mode: "none"
        },
        columns: [
				  {
					  caption: "№",
					  dataField: "Id",
					  width: 50
				  },
                  {
                	  caption: "Наименование меморандума, соглашения, проекта",
                	  dataField: "Name",
                	  allowFiltering: false,
                      allowSorting: false,
                      cellTemplate: function (container, options) {
                    	  var block = options.value + '<span>Отв.: ' + options.data.Responsible +'</span>';
                          $('<div class="up_title">').append(block).appendTo(container);
                      }
                  },
                  {
                	  caption: "Сроки реализации",
                	  dataField: "Date",
                	  cellTemplate: function (container, options) {
                          $('<div class="up_date">').append(options.value?options.value:'-').appendTo(container);
                      }
                  },
                  {
                	  caption: "Иностранный партнер",
                	  dataField: "Partner",
                	  cellTemplate: function (container, options) {
                          $('<div class="up_partner">').append(options.value?options.value:'-').appendTo(container);
                      }
                  },
                  {
                	  caption: "Общая стоимость проекта",
                	  dataField: "Sum",
                	  format: "fixedPoint",
                	  cellTemplate: function (container, options) {
                          $('<div class="up_price">').append(options.value?options.value:'-').appendTo(container);
                      }
                  },
                  {
                	  caption: "Ход реализации по механизму",
                	  dataField: "Status",
                	  cellTemplate: function (container, options) {
                          $('<div class="up_desc">').append(options.value?options.value:'-').appendTo(container);
                      }
                  },
     	]
    });


});