<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

if (!CModule::IncludeModule('crm')) {
    ShowError(GetMessage('CRM_MODULE_NOT_INSTALLED'));
    return;
}

$COUNTRY_UF = 'UF_CRM_1529991824';
$MID = 'UF_CRM_1530258582';

function UpsendJsonAnswer($result)
{
    global $APPLICATION;

    $APPLICATION->RestartBuffer();
    header('Content-Type: application/json');

    echo \Bitrix\Main\Web\Json::encode($result);

    CMain::FinalActions();
    die();
}

// Статусы
$dbStatus = \CCrmStatus::GetList(['SORT' => 'ASC'],['ENTITY_ID'=>'DEAL_STAGE']);
while ($rsStatus = $dbStatus->fetch())
{
    $arStatus[$rsStatus['STATUS_ID']] = $rsStatus;
}


// Пользовательские свойства
global $DB;
$rsField = CUserTypeEntity::GetList( array($by=>$order), array("ENTITY_ID"=>"CRM_DEAL") );
while($arField = $rsField->Fetch())
{
    $fieldName = $DB->Query("
        SELECT EDIT_FORM_LABEL AS NAME
        FROM b_user_field_lang
        WHERE USER_FIELD_ID=".$arField['ID']."
    ")->Fetch();
    $arField['NAME'] = $fieldName['NAME'];

    if($arField['USER_TYPE_ID'] == 'enumeration'){
            $rsMultiField = CUserFieldEnum::GetList(array(), array(
                "USER_FIELD_ID" => $arField["ID"],
            ));
            while ($arMultiField = $rsMultiField->GetNext()){
                $arField['LIST'][$arMultiField['ID']] = $arMultiField;
            }
    }
    $arUserField[$arField['FIELD_NAME']] = $arField;
}


// Все сделки
$arCountryReport = [];
$dbDeal = \CCrmDeal::GetListEx(
    [],
    ['CATEGORY_ID'=>0, 'UF_CRM_1529991824'=>186],
    false,
    false,
    ['*','UF_*']
);

$i=0;
while ($rsDeal = $dbDeal->Fetch())
{
   // echo "<pre>";print_r($rsDeal);echo"</pre>";
    $i++;
    $arCountryReport[$i]['Id'] = $i;
    $arCountryReport[$i]['Name'] = $rsDeal['TITLE'];
    $arCountryReport[$i]['Responsible'] = $arUserField[$MID]['LIST'][$rsDeal[$MID]]['VALUE'];
    $arCountryReport[$i]['Date'] = $rsDeal['UF_CRM_1529661397'];
    $arCountryReport[$i]['Partner'] = $rsDeal['COMPANY_TITLE'];
    $arCountryReport[$i]['Sum'] = $rsDeal['OPPORTUNITY_ACCOUNT'];
    $arCountryReport[$i]['FactSum'] = ($rsDeal['UF_CRM_1530603763871'] + $rsDeal['UF_CRM_1530603663343'] + $rsDeal['UF_CRM_1530603688894'] + $rsDeal['UF_CRM_1530603706770'] + $rsDeal['UF_CRM_1530603719820']);
    $arCountryReport[$i]['Status'] = $arStatus[$rsDeal['STAGE_ID']]['NAME'];
}

$arResult = $arCountryReport;

$this->IncludeComponentTemplate();


?>