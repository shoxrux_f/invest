<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	'NAME' => "Upsolutions: Отчеты",
	'DESCRIPTION' => "Отчетность",
	'ICON' => '/images/icon.gif',
	'SORT' => 20,
	'PATH' => array(
		'ID' => 'crm',
		'NAME' => "Отчеты"
	),
	'CACHE_PATH' => 'Y'
);
?>